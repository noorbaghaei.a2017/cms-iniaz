<?php

namespace Modules\Store\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\Category;
use Modules\Core\Http\Controllers\HasCategory;
use Modules\Core\Http\Controllers\HasQuestion;
use Modules\Core\Http\Requests\CategoryRequest;
use Modules\Question\Entities\Question;
use Modules\Question\Http\Requests\QuestionRequest;
use Modules\Store\Entities\Store;
use Modules\Store\Http\Requests\StoreRequest;
use Modules\Store\Transformers\StoreCollection;

class StoreController extends Controller
{
    use HasQuestion,HasCategory;

    protected $entity;

    protected $class;


//category

    protected $route_categories_index='store::categories.index';
    protected $route_categories_create='store::categories.create';
    protected $route_categories_edit='store::categories.edit';
    protected $route_categories='store.categories';


//question

    protected $route_questions_index='store::questions.index';
    protected $route_questions_create='store::questions.create';
    protected $route_questions_edit='store::questions.edit';
    protected $route_questions='stores.index';


//notification

    protected $notification_store='store::stores.store';
    protected $notification_update='store::stores.update';
    protected $notification_delete='store::stores.delete';
    protected $notification_error='store::stores.error';

    public function __construct()
    {
        $this->entity=new Store();

        $this->class=Store::class;

        $this->middleware('permission:store-list');
        $this->middleware('permission:store-create')->only(['create','store']);
        $this->middleware('permission:store-edit' )->only(['edit','update']);
        $this->middleware('permission:store-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

            try {

                $items=$this->entity->get();

                $stores = new StoreCollection($items);

                $data= collect($stores->response()->getData())->toArray();

                return view('core::response.index',compact('data'));
            }catch (\Exception $exception){
                return abort('500');
            }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            $categories=Category::latest()->where('model',Store::class)->get();
            return view('store::stores.create',compact('categories'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(StoreRequest $request)
    {
        try {

            $this->entity->user=auth('web')->user()->id;
            $this->entity->title=$request->input('title');
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->text=$request->input('text');
            $this->entity->order=orderInfo($request->input('order'));
            $this->entity->category=Category::whereToken($request->input('category'))->first()->id;
            $this->entity->longitude=$request->input('longitude');
            $this->entity->latitude=$request->input('latitude');
            $this->entity->google_map=$request->input('map');
            $this->entity->token=tokenGenerate();

            $saved=$this->entity->save();

            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            if(!$saved){
                return redirect()->back()->with('error',__('store::stores.error'));
            }else{
                return redirect(route("stores.index"))->with('message',__('store::stores.store'));
            }
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
            !isset($request->title)
            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));
                return view('store::stores.index',compact('items'));
            }
            $items=$this->entity
                ->where("title",'LIKE','%'.trim($request->title).'%')
                ->paginate(config('cms.paginate'));
            return view('store::stores.index',compact('items','request'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }


    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($token)
    {
        try {
            $categories=Category::latest()->where('model',Store::class)->get();
            $item=$this->entity->whereToken($token)->first();
            return view('store::stores.edit',compact('item','categories'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(StoreRequest $request, $token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $updated=$this->entity->update([
                'user'=>auth('web')->user()->id,
                "slug"=>null,
                "title"=>$request->input('title'),
                "excerpt"=>$request->input('excerpt'),
                "text"=>$request->input('text'),
                "order"=>orderInfo($request->input('order')),
                "category"=>Category::whereToken($request->input('category'))->first()->id,
                "longitude"=>$request->input('longitude'),
                "latitude"=>$request->input('latitude'),
                "google_map"=>$request->input('map'),

            ]);
            $this->entity->replicate();

            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!$updated){
                return redirect()->back()->with('error',__('store::stores.error'));
            }else{
                return redirect(route("stores.index"))->with('message',__('store::stores.update'));
            }

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            if($this->entity->Hasmedia(config('cms.collection-image'))){
                destroyMedia($this->entity,config('cms.collection-image'));
            }
            $deleted=$this->entity->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('store::stores.error'));
            }else{
                return redirect(route("stores.index"))->with('message',__('store::stores.delete'));
            }

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

}
