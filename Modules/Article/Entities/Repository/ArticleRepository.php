<?php


namespace Modules\Article\Entities\Repository;


use Modules\Article\Entities\Article;

class ArticleRepository implements ArticleRepositoryInterface
{
    public function getAll()
    {
        return Article::latest()->get();
    }

}
