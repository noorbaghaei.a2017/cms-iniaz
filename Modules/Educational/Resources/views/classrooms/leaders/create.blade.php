@include('core::layout.modules.leader.create',[

    'title'=>__('core::leaders.create'),
    'parent'=>'educational',
    'model'=>'classroom',
    'directory'=>'classrooms',
    'collect'=>__('core::leaders.collect'),
    'singular'=>__('core::leaders.singular'),
   'store_route'=>['name'=>'classroom.leader.store'],

])








