<?php


namespace Modules\Educational\Entities\Repository;


interface ClassRoomRepositoryInterface
{

    public function getAll();
}
