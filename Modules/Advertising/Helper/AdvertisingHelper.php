<?php

namespace Modules\Advertising\Helper;


class AdvertisingHelper{



    public static function status($status){
        switch ($status){
            case 1 :
                return '<span class="alert-warning">در انتظار تایید</span>';
                break;
            case 2 :
                return '<span class="alert-success">منتشر شد</span>';
                break;
            default:
                return '<span class="alert-danger">خطای سیستمی</span>';
                break;

        }
    }
    public static function statusValue($status)
    {
        switch ($status) {
            case 1 :
                return 'در انتظار تایید';
                break;
            case 2 :
                return 'منتشر شد';
                break;

            default:
                return 'خطای سیستمی';
                break;

        }
    }
    public static function publish($publish){
        switch ($publish){
            case 1 :
                return '<span class="alert-warning">عمومی</span>';
                break;
            case 2 :
                return '<span class="alert-success">فوری</span>';
                break;
            default:
                return '<span class="alert-danger">خطای سیستمی</span>';
                break;

        }
    }
    public static function publishValue($publish)
    {
        switch ($publish) {
            case 1 :
                return 'عمومی';
                break;
            case 2 :
                return 'فوری';
                break;

            default:
                return 'خطای سیستمی';
                break;

        }
    }

}
