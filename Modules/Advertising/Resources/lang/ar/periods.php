<?php
return [
    "text-create"=>"you can create your period",
    "text-edit"=>"you can edit your period",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"periods list",
    "error"=>"error",
    "singular"=>"period",
    "collect"=>"periods",
    "permission"=>[
        "period-full-access"=>"period full access",
        "period-list"=>"periods list",
        "period-delete"=>"period delete",
        "period-create"=>"period create",
        "period-edit"=>"edit period",
    ]


];
