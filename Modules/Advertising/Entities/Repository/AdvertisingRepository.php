<?php


namespace Modules\Advertising\Entities\Repository;


use Modules\Advertising\Entities\Advertising;

class AdvertisingRepository implements AdvertisingRepositoryInterface
{

    public function getAll()
    {
        return Advertising::latest()->get();
    }
}
