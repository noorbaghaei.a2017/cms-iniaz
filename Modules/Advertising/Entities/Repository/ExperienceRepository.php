<?php


namespace Modules\Advertising\Entities\Repository;


use Modules\Advertising\Entities\Experience;

class ExperienceRepository implements ExperienceRepositoryInterface
{

    public function getAll()
    {
        return Experience::latest()->get();
    }
}
