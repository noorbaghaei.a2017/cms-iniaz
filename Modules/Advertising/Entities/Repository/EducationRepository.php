<?php


namespace Modules\Advertising\Entities\Repository;


use Modules\Advertising\Entities\Education;

class EducationRepository implements EducationRepositoryInterface
{

    public function getAll()
    {
       return Education::latest()->get();
    }
}
