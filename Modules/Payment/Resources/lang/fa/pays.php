<?php

return [
    "store"=>"ثبت با موفقیت انجام شد.",
    "delete"=>"حذف با موفقیت انجام شد",
    "update"=>"بروز رسانی با موفقیت انجام شد",
    "index"=>"لیست پرداختی ها",
    "singular"=>"پرداختی ",
    "collect"=>"پرداختی ها",
    "permission"=>[
        "payment-full-access"=>"دسترسی کامل به پرداختی ها",
        "payment-list"=>"لیست پرداختی ها",
        "payment-delete"=>"حذف درگاه پرداختی",
        "payment-create"=>"ایجاد پرداختی ",
        "payment-edit"=>"ویرایش پرداختی",
    ]
];
