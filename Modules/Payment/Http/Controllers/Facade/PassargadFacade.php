<?php


namespace Modules\Payment\Http\Controllers\Facade;


use Illuminate\Support\Facades\Facade;

class PassargadFacade extends Facade
{

    protected static function getFacadeAccessor()
    {
        return 'passargad';
    }
}
