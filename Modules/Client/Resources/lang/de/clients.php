<?php
return [
    "store"=>"Registrierung erfolgreich abgeschlossen",
    "error"=>"Das System ist auf ein Problem gestoßen",
    "error-password"=>"error password",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"members list",
    "singular"=>"client",
    "collect"=>"clients",
    "permission"=>[
        "client-full-access"=>"client full access",
        "client-list"=>"client list",
        "client-delete"=>"client delete",
        "client-create"=>"client create",
        "client-edit"=>"edit client",
    ]
];
