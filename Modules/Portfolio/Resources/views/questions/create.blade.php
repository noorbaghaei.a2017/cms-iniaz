@include('core::layout.modules.question.create',[

    'title'=>__('core::categories.create'),
    'token'=>$token,
    'parent'=>'portfolio',
    'model'=>'portfolio',
    'directory'=>'portfolios',
    'collect'=>__('core::categories.collect'),
    'singular'=>__('core::categories.singular'),
   'store_route'=>['name'=>'portfolio.question.store','param'=>'portfolio'],

])








