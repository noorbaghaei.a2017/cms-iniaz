<?php

namespace Modules\Video\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Analyzer;
use Modules\Core\Entities\Translate;
use Modules\Core\Entities\User;
use Modules\Core\Helper\Trades\TimeAttribute;
use Modules\Question\Entities\Question;
use Modules\Seo\Entities\Seo;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Tags\HasTags;

class Video extends Model implements HasMedia
{
    use HasTags,TimeAttribute,HasMediaTrait;

    protected $fillable = ['title','href','excerpt','videoable_id','videoable_type','token','order','user','status','category','text'];


    public function getRouteKeyName()
    {
        return multiRouteKey();
    }

    public function analyzer()
    {
        return $this->morphOne(Analyzer::class, 'analyzerable');
    }
    public function questions()
    {
        return $this->morphMany(Question::class, 'questionable');
    }
    public function videos()
    {
        return $this->morphMany(Video::class, 'videoable');
    }
    public function seo()
    {
        return $this->morphOne(Seo::class, 'seoable');
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function translates()
    {
        return $this->morphMany(Translate::class, 'translateable');
    }

    public function videoable()
    {
        return $this->morphTo();
    }
    public function user_info()
    {
        return $this->belongsTo(User::class,'user','id');
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('medium')
            ->width(500)
            ->height(300)
            ->performOnCollections(config('cms.collection-image'));


    }
}
