<?php

namespace Modules\Question\Transformers;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Modules\Question\Entities\Question;

class QuestionCollection extends ResourceCollection
{
    public $collects = Question::class;
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(
                function ( $item ) {
                    return new QuestionResource($item);
                }
            ),
            'filed' => [
                'title','update_date','create_date'
            ],
            'public_route'=>[

                [
                    'name'=>'questions.create',
                    'param'=>[null],
                    'icon'=>config('cms.icon.add'),
                    'title'=>__('cms.add'),
                    'class'=>'btn btn-sm text-sm text-sm-center btn-primary pull-right',
                    'method'=>'GET',
                ]
            ],
            'private_route'=>
                [
                    [
                        'name'=>'questions.edit',
                        'param'=>[
                            'question'=>'token'
                        ],
                        'icon'=>config('cms.icon.edit'),
                        'title'=>__('cms.edit'),
                        'class'=>'btn btn-warning btn-sm text-sm',
                        'modal'=>false,
                        'method'=>'GET',
                    ],
                    [
                        'name'=>'questions.destroy',
                        'param'=>[
                            'question'=>'token'
                        ],
                        'icon'=>config('cms.icon.delete'),
                        'title'=>__('cms.delete'),
                        'class'=>'btn btn-danger btn-sm text-sm text-white',
                        'modal'=>true,
                        'modal_type'=>'destroy',
                        'method'=>'DELETE',
                    ],
                    [
                        'name'=>'questions.detail',
                        'param'=>[null],
                        'icon'=>config('cms.icon.detail'),
                        'title'=>__('cms.detail'),
                        'class'=>'btn btn-primary btn-sm text-sm text-white',
                        'modal'=>true,
                        'modal_type'=>'detail',
                        'method'=>'GET',
                    ]
                ],
            'search_route'=>[

                'name'=>'search.question',
                'filter'=>[
                    'title','slug'
                ],
                'icon'=>config('cms.icon.add'),
                'title'=>__('cms.add'),
                'class'=>'btn btn-sm text-sm text-sm-center btn-primary pull-right',
                'method'=>'POST',

            ],
            'language_route'=>true,
            'class_model'=> 'question',
            'count' => $this->collection->count(),
            'links' => [
                'self' => 'link-value',
            ],
            'collect'=>__('question::questions.collect'),
            'title'=>__('question::questions.index'),
        ];
    }
}
