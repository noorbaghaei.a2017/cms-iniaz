<?php

namespace Modules\Request\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Request\Entities\Request;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('permissions')->whereModel(Request::class)->delete();

        Permission::create(['name'=>'request-list','model'=>Request::class,'created_at'=>now()]);
        Permission::create(['name'=>'request-create','model'=>Request::class,'created_at'=>now()]);
        Permission::create(['name'=>'request-edit','model'=>Request::class,'created_at'=>now()]);
        Permission::create(['name'=>'request-delete','model'=>Request::class,'created_at'=>now()]);
    }
}
