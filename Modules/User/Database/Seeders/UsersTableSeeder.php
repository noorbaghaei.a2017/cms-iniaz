<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('users')->delete();


        $users=[
            [
                'first_name'=>"amin",
                'last_name'=>"nourbaghaei",
                'username'=>"nourbaghaei",
                'direction'=>"ltr",
                'lang'=>"en",
                'name'=>"amin",
                'mobile'=>"9195995044",
                'country' => '+98',
                'token'=>Str::random(),
                "email"=>"noorbaghaei.a2017@gmail.com",
                "password"=>Hash::make('secret_nourbaghaei'),
                "created_at"=>now()
            ],
            [
                'first_name'=>"helper",
                'last_name'=>"company",
                'username'=>"helper",
                'name'=>"helper",
                'direction'=>"rtl",
                'lang'=>"fa",
                'mobile'=>"9195995040",
                'country' => '+98',
                'token'=>Str::random(),
                "email"=>"support@helper.net",
                "password"=>Hash::make('secret_helper'),
                "created_at"=>now()
            ],
            [
                'first_name'=>"iniaz",
                'last_name'=>"company",
                'username'=>"iniaz",
                'name'=>"iniaz",
                'direction'=>"ltr",
                'lang'=>"de",
                'mobile'=>"1781436854",
                'country' => '+49',
                'token'=>Str::random(),
                "email"=>"support@iniaz.net",
                "password"=>Hash::make('secret_iniaz'),
                "created_at"=>now()
            ]
        ];


        DB::table('users')->insert($users);

    }
}
