<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Helper\Trades\TimeAttribute;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class UserServices extends Model implements HasMedia
{
    use HasMediaTrait,TimeAttribute;

    protected $table = 'user_services';

    protected $fillable = ['id','user','service','title','text','excerpt','country','city','google_map','email','address','phone'];

    public function translates()
    {
        return $this->morphMany(Translate::class, 'translateable');
    }

    // public function field()
    // {
    //     return $this->hasMany(UserServiceField::class,'service','id');
    // }

    // public function drink()
    // {
    //     return $this->hasMany(UserServiceField::class,'service','id');
    // }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('medium')
            ->width(418)
            ->height(200)
            ->performOnCollections(config('cms.collection-image'));

        $this->addMediaConversion('thumb')
            ->width(70)
            ->height(60)
            ->performOnCollections(config('cms.collection-image'));
    }
}
