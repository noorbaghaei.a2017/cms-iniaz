@include('core::layout.header')
<body>
<div class="app" id="app">

    <!-- ############ LAYOUT START-->

    <div id="aside" class="app-aside fade nav-dropdown black">
        <div class="navside dk" data-layout="column">

            <div class="navbar no-radius">
                <div>
                    @if(env('SET_CUSTOMER'))

                        <img src="{{asset('assets/images/ads/roshan-logo.jpeg')}}" width="200"  style="margin: 0 auto">
                    @else
                        <img src="{{asset('assets/images/ads/alo-peyk.jpg')}}" width="200"  style="margin: 0 auto">

                    @endif
                    <br>
                </div>
                <a href="#" class="navbar-brand">


                    @if(!$setting->Hasmedia('logo'))
                        <img src="{{asset('assets/images/logo.png')}}" alt="{{config('app.name')}}" >
                    @else
                        <img src="{{$setting->getFirstMediaUrl('logo')}}" alt="{{config('app.name')}}" >
                    @endif

                    <span class="hidden-folded inline">{{$setting->name}}</span>
                    <br>
                    <span class="hidden-folded inline">{{number_format($total_orders)}}   تومان </span>
                    <br>
                    <span class="hidden-folded inline">{{showIp()}}</span>
                </a>
            </div>

            @include('core::layout.list-module')
        </div>
    </div>

    <!-- content -->
    <div id="content" class="app-content box-shadow-z2 pjax-container" role="main">
        <div class="app-header hidden-lg-up black lt b-b">
            <div class="navbar" data-pjax>
                <a data-toggle="modal" data-target="#aside" class="navbar-item pull-left hidden-lg-up p-r m-a-0">
                    <i class="ion-navicon"></i>
                </a>
                <div class="navbar-item pull-left h5" id="pageTitle">@yield('pageTitle')</div>
                <ul class="nav navbar-nav pull-right">
                    <li class="nav-item"><a href="{{route('meeting.website')}}" target="_blank" class="btn btn-sm text-sm btn-success text-white m-t-1">{{__('cms.chat-room')}}</a></li>
                    <li class="nav-item"><a href="{{route('front.website')}}" target="_blank" class="btn btn-sm text-sm btn-primary text-white m-t-1">{{__('cms.website')}}</a></li>
                    <li class="nav-item dropdown">
                        <a class="nav-link clear" data-toggle="dropdown">
                            <span class="avatar w-32">
                                <img src="{{asset('assets/images/a3.jpg')}}" class="w-full rounded" alt="#">
                            </span>
                        </a>
                        <div class="dropdown-menu w dropdown-menu-scale pull-right">
                            <a class="dropdown-item" href="{{route('user.profile',['user'=>auth('web')->user()->token])}}">
                                <span>{{__('cms.profile')}}</span>
                            </a>
                            <a class="dropdown-item"
                               onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">{{__('cms.logout')}}</a>
                            <form id="frm-logout" action="{{route('logout')}}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </li>
                </ul>
            </div>

        </div>
        @include('core::layout.copy')
        <div class="app-body">

            <!-- ############ PAGE START-->

            <div class="app-body-inner">
                <div class="row-col">
                    <div class="col-xs-3 w-lg modal fade aside aside-sm" id="list">
                        <div class="row-col success">
                            <!-- header -->
                            <div>
                                <div class="p-a">

                                        <div class="input-group input-group-sm p-x-sm dker rounded">
                                            <input id="search-contact"  type="text" class="form-control no-bg no-border text-white" placeholder="جستجو" required="">
                                            <span class="input-group-btn">

				              	</span>
                                        </div>

                                </div>
                                <div class="nav text-center p-b">
                                    <a href="#" class="nav-link m-x" data-toggle="tooltip" title="مخاطب ها"><i class="ion-person-stalker"></i></a>
                                </div>
                            </div>
                            <!-- / -->
                            <!-- flex content -->
                            <div class="row-row">
                                <div class="row-body scrollable hover">
                                    <div class="row-inner">
                                        <!-- left content -->
                                        <div class="list inset" data-ui-list="active">
                                            <div class="p-x text-dark m-t m-b-sm text-sm"> مخاطبین</div>

                                            @foreach($clients as $item)
                                            <div class="list-item chat"  data-user="{{$item->id}}">
                                                <div class="list-left">
				      			        <span class="w-40 avatar circle indigo">
				      			          <i class="on b-white avatar-right"></i>
                                                @if(!$item->Hasmedia('images'))
                                                <img src="{{asset('img/no-img.gif')}}" >
                                            @else
                                                <img src="{{$item->getFirstMediaUrl('images')}}">
                                            @endif

				      			        </span>
                                                </div>
                                                <div class="list-body">
                                                    @if(userChatUnreadCount($item->id)>0)

                                                        <span class="pull-right text-xs label rounded lt unread">
                                                            {{userChatUnreadCount($item->id)}}
				      			                         </span>
                                                    @endif

                                                    <div class="item-title">
                                                        <a href="#" class="_500">{{$item->full_name}} </a>
                                                    </div>
                                                    <small class="block text-muted text-ellipsis">
                                                        {{$item->info->about}}
                                                    </small>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                        <!-- / -->
                                    </div>
                                </div>
                            </div>
                            <!-- / -->

                        </div>
                    </div>
                    <div class="col-xs-6" id="detail" style="opacity: 0">
                        <div class="row-col dark-light">
                            <!-- header -->
                            <div class="dark-white b-b">
                                <div class="navbar">

                                    <!-- nabar right -->
                                    <ul class="nav navbar-nav pull-right m-l">
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tooltip" data-placement="bottom" title="تماس تلفنی">
						            <span class="btn btn-sm btn-icon rounded success">
							      		<i class="fa fa-phone"></i>
							        </span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tooltip" data-placement="bottom" title="تماس تصویری">
						            <span class="btn btn-sm btn-icon rounded success">
							      		<i class="fa fa-video-camera"></i>
							        </span>
                                            </a>
                                        </li>
                                    </ul>
                                    <!-- / navbar right -->

                                    <a data-toggle="modal" data-target="#profile" data-ui-modal class="m-r-0 m-l navbar-item pull-right hidden-lg-up">
							<span class="btn btn-sm btn-icon rounded success">
					      		<i class="ion-person"></i>
					        </span>
                                    </a>
                                    <a data-toggle="modal" data-target="#list" data-ui-modal class="navbar-item pull-left hidden-md-up">
					    	<span class="btn btn-sm btn-icon success">
					      		<i class="fa fa-list"></i>
					      	</span>
                                    </a>

                                    <span id="chat-title" class="navbar-item text-md text-ellipsis">

				        </span>
                                </div>
                            </div>
                            <!-- / -->
                            <!-- flex content -->
                            <div class="row-row">
                                <div class="row-body">
                                    <div id="chat-list" class="row-inner">

                                    </div>
                                </div>
                            </div>
                            <!-- / -->
                            <div class="p-a b-t dark-white">

                                    <div class="input-group">
                                        <input id="message" type="text" class="form-control" placeholder="متن خود را تایپ کنید" autocomplete="off">
                                        <span id="sendMessage" class="input-group-btn" >
				            	<span  class="btn white b-a no-shadow" type="button">
				            		<i class="fa fa-send text-success"></i>
				            	</span>
				          	</span>
                                    </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-xs-3 w-md modal fade aside aside-md aside-md-right" id="profile" style="opacity: 0">
                        <div class="row-col dark-white b-l">
                            <!-- flex content -->
                            <div class="row-row">
                                <div class="row-body scrollable hover">
                                    <div class="row-inner">
                                        <!-- content -->
                                        <div class="p-a-lg text-center">
                                                <img id="profile-image" src="{{asset('img/no-img.gif')}}" class="w-full circle" alt=".">

                                            <span class="text-md m-t block" id="profile-name"></span>
                                            <small id="profile-about" class="text-muted"> </small>
                                            <div class="block clearfix m-t">
                                                <a href="#" class="btn btn-icon btn-social rounded btn-sm">
                                                    <i class="fa fa-instagram"></i>
                                                    <i class="fa fa-instagram pink"></i>
                                                </a>
                                                <a href="#" class="btn btn-icon btn-social rounded btn-sm">
                                                    <i class="fa fa-whatsapp"></i>
                                                    <i class="fa fa-whatsapp green"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="p-a-md">
                                            <ul class="nav">

                                                <li class="nav-item m-b-xs">
                                                    <a class="nav-link text-muted block">
						                	<span class="pull-right text-sm">
						                		<i class="fa fa-fw fa-phone"></i>
						                	</span>
                                                        <span id="profile-mobile"></span>
                                                    </a>
                                                </li>
                                                <li class="nav-item m-b-xs">
                                                    <a class="nav-link text-muted block">
						                	<span class="pull-right text-sm">
						                		<i class="fa fa-fw fa-envelope"></i>
						                	</span>
                                                        <span id="profile-email"></span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- / -->
                                    </div>
                                </div>
                            </div>
                            <!-- / -->
                        </div>
                    </div>
                </div>
            </div>

            <!-- ############ PAGE END-->

        </div>
    </div>
    <!-- / -->


    <!-- ############ LAYOUT END-->
</div>

@include('core::layout.footer')

