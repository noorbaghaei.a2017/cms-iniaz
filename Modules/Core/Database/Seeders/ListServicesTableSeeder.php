<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ListServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('list_services')->delete();

        $list_services=[
            [
                'title' => 'restaurant',
                'slug' => 'restaurant',
                'created_at' =>now(),
            ],

        ];

        DB::table('list_services')->insert($list_services);
    }
}
