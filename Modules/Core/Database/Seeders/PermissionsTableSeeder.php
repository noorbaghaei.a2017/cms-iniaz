<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Core\Entities\Award;
use Modules\Core\Entities\Contact;
use Modules\Core\Entities\Currency;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('permissions')->whereModel(Contact::class)->delete();

        Permission::create(['name'=>'contact-list','model'=>Contact::class,'created_at'=>now()]);
        Permission::create(['name'=>'contact-edit','model'=>Contact::class,'created_at'=>now()]);
        Permission::create(['name'=>'contact-delete','model'=>Contact::class,'created_at'=>now()]);


        DB::table('permissions')->whereModel(Currency::class)->delete();

        Permission::create(['name'=>'currency-list','model'=>Currency::class,'created_at'=>now()]);
        Permission::create(['name'=>'currency-create','model'=>Currency::class,'created_at'=>now()]);
        Permission::create(['name'=>'currency-edit','model'=>Currency::class,'created_at'=>now()]);


        DB::table('permissions')->whereModel(Award::class)->delete();

        Permission::create(['name'=>'award-list','model'=>Award::class,'created_at'=>now()]);
        Permission::create(['name'=>'award-create','model'=>Award::class,'created_at'=>now()]);
        Permission::create(['name'=>'award-edit','model'=>Award::class,'created_at'=>now()]);
        Permission::create(['name'=>'award-delete','model'=>Award::class,'created_at'=>now()]);
    }
}
