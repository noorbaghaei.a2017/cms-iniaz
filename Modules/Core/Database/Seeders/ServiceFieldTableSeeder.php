<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Core\Entities\ListServices;

class ServiceFieldTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('service_field')->delete();

        $service_field=[
            [
                'service' => 1,
                'title' => 'food',
                'created_at' =>now(),
            ],
            [
                'service' => 1,
                'title' => 'drink',
                'created_at' =>now(),
            ]

        ];

        DB::table('service_field')->insert($service_field);
    }
}
