@extends('template.app')
@section('content')

<div class="content">
                    <!--  section  -->
                    <section class="parallax-section small-par" data-scrollax-parent="true">
                        <div class="bg"  data-bg="{{asset('template/images/bg/hero/4.jpg')}}" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="overlay op7"></div>
                        <div class="container">
                            <div class="error-wrap">
                                <div class="bubbles">
                                    <h2>404</h2>
                                </div>
                                <p>متأسفیم ، اما صفحه مورد نظر شما یافت نشد.</p>
                                <div class="clearfix"></div>
                                <form action="#">
                                    <input name="se" id="se" type="text" class="search" placeholder="جستجو.." value="جستجو...">
                                    <button class="search-submit color-bg" id="submit_btn"><i class="fal fa-search"></i> </button>
                                </form>
                                <div class="clearfix"></div>
                                <p>یا</p>
                                <a href="index.html" class="btn   color2-bg">بازگشت به صفحه اصلی<i class="far fa-home-alt"></i></a>
                            </div>
                        </div>
                    </section>
                    <!--  section  end-->
                </div>
   

@endsection

