@extends('template.app')

@section('content')

    <!-- Inne Page Banner Area Start Here -->
    <section class="inner-page-banner2 bg-common inner-page-top-margin bg--gradient-bottom-60" data-bg-image="{{asset('template/img/figure/figure3.jpg')}}"></section>
    <!-- Inne Page Banner Area End Here -->

    <!-- Listing Area Start Here -->
    <section class="single-listing-wrap-layout1 padding-top-6 padding-bottom-7 bg--accent">
        <div class="container">
            <div class="single-listing-summary-wrap1">
                <div class="row">
                    <div class="col-xl-8 col-lg-6">
                        <div class="single-listing-summary1">
                            <div class="row no-gutters">
                                <div class="col-12">
                                    <div class="media">
                                        <figure>
                                            @if(!$item->Hasmedia('logo'))
                                                <img src="{{asset('img/no-img.gif')}}" alt="Listing"  width="80">
                                            @else
                                                <img src="{{$item->getFirstMediaUrl('logo')}}" alt="Listing" width="80">
                                            @endif
                                        </figure>
                                        <div class="media-body space-md">
                                            <h2 class="item-title">{{$item->company->name}}</h2>
                                            <p>{{$item->company->about}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-6 col-md-4 col-sm-6 col-12">
                                    <div class="entry-meta-date"><i class="far fa-calendar-alt"></i>اردیبهشت 1398</div>
                                </div>
                                <div class="col-xl-4 col-lg-6 col-md-4 col-sm-6 col-12">
                                    <div class="entry-meta-location"><i class="fas fa-map-marker-alt"></i>مسافرت و عشق</div>
                                </div>
                                <div class="col-xl-3 col-lg-12 col-md-4 col-sm-12 col-12">
                                    <ul class="entry-meta-rating">
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><span>8.4<span> / 10</span></span> </li>
                                    </ul>
                                </div>
                                <div class="col-xl-2 d-none d-xl-block">
                                    <div class="item-status">
                                        <div class="status-save">صرفه جویی 15٪</div>
                                        <div class="status-open">باز</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6">
                        <div class="single-listing-share1">
                            <div class="row gutters-15">
                                <div class="col-4">
                                    <a href="#" class="item-btn"><i class="fas fa-flag"></i>گزارش</a>
                                </div>
                                <div class="col-4">
                                    <a href="#" class="item-btn"><i class="fas fa-share-alt"></i>اشتراک</a>
                                </div>
                                <div class="col-4">
                                    <a href="#" class="item-btn"><i class="fas fa-heart"></i>مورد علاقه</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8">
                    <div class="single-listing-box-layout1">
                        <div class="listygo-text-box listing-details-info">
                            <ul class="nav nav-tabs">
                                <li class="nav-item">
                                    <a href="#one" data-toggle="tab" aria-expanded="false" class="active">توضیحات</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#two" data-toggle="tab" aria-expanded="false">امکانات</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#three" data-toggle="tab" aria-expanded="false">ویدیو ها</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade active show" id="one">
                                    <div class="row gutters-10">
                                        <div class="col-md-8">
                                            <div class="gallery-box-layout1">
                                                <img src="{{asset('template/img/listing/listing-18.jpg')}}" alt="listing" class="img-fluid">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="gallery-box-layout1">
                                                <img src="{{asset('template/img/listing/listing-19.jpg')}}" alt="listing" class="img-fluid">
                                            </div>
                                            <div class="gallery-box-layout1">
                                                <img src="{{asset('template/img/listing/listing-20.jpg')}}" alt="listing" class="img-fluid">
                                            </div>
                                            <div class="gallery-box-layout1 zoom-gallery">
                                                <img src="{{asset('template/img/listing/listing-21.jpg')}}" alt="listing" class="img-fluid">
                                                <a href="{{asset('template/img/listing/listing-15.jpg')}}" class="popup-zoom">15<i class="fa fa-plus"
                                                                                                                                   aria-hidden="true"></i></a>
                                                <a href="{{asset('template/img/listing/listing-16.jpg')}}" class="popup-zoom d-none"></a>
                                                <a href="{{asset('template/img/listing/listing-17.jpg')}}" class="popup-zoom d-none"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <h2 class="tab-content-title">معرفی {{$item->company->name}}</h2>

                                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم استلورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم استلورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم استلورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است</p>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="two">

                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="three">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-6 col-6">
                                            <div class="video-info">
                                                <img src="{{asset('template/img/listing/listing-1-2.jpg')}}" alt="video">
                                                <a class="play-btn popup-youtube" href="http://www.youtube.com/watch?v=1iIZeIy7TqM">
                                                    <i class="fas fa-play"></i>
                                                </a>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="listygo-text-box listing-details-review">
                            <h3 class="inner-item-heading">نظرات</h3>
                            <div class="total-review"><span>(05)</span> امتیاز</div>
                            <ul class="review-items">
                                <li class="post-no-one">
                                    <div class="media">
                                        <figure><img src="{{asset('template/img/blog/review1.jpg')}}" alt="Review"></figure>
                                        <div class="media-body">
                                            <span class="review-title">تجربه تور بسیار معروف !!!</span>
                                            <h4 class="reviewer-name">طاهر خان<span class="review-date">اردیبهشت 1398</span></h4>
                                            <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.</p>
                                            <ul class="entry-meta">
                                                <li><a href="#"><i class="fas fa-thumbs-up"></i><span>25</span>
                                                        پسندیده</a></li>
                                                <li><a href="#"><i class="fas fa-reply"></i><span>19</span> اشتراک</a></li>
                                            </ul>
                                            <ul class="item-rating">
                                                <li class="star-one"><i class="fas fa-star"></i></li>
                                                <li class="star-two"><i class="fas fa-star"></i></li>
                                                <li class="star-three"><i class="fas fa-star"></i></li>
                                                <li class="star-four"><i class="fas fa-star"></i></li>
                                                <li class="star-five"><i class="fas fa-star"></i></li>
                                                <li><span>(عالی!)</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>

                            </ul>
                        </div>
                        <div class="listygo-text-box listing-details-leave-review">
                            <h3 class="inner-item-heading">نظر خود را بگید </h3>
                            <div class="rate-wrapper">
                                <div class="rate-label"> امتیاز کلی شما به این آگهی</div>
                                <div class="rate">
                                    <div class="rate-item"><i class="fa fa-star" aria-hidden="true"></i></div>
                                    <div class="rate-item"><i class="fa fa-star" aria-hidden="true"></i></div>
                                    <div class="rate-item"><i class="fa fa-star" aria-hidden="true"></i></div>
                                    <div class="rate-item"><i class="fa fa-star" aria-hidden="true"></i></div>
                                    <div class="rate-item"><i class="fa fa-star" aria-hidden="true"></i></div>
                                </div>
                            </div>
                            <form class="contact-form-box" id="contact-form2">
                                <div class="row">
                                    <div class="col-12 form-group">
                                        <label>عنوان نظر شما</label>
                                        <input type="text" placeholder="" class="form-control" name="title"
                                               data-error="این فیلد ضروری است" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <label>نظر شما</label>
                                        <textarea placeholder="" class="textarea form-control" name="message" rows="7"
                                                  cols="20" data-error="این فیلد ضروری است" required></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="col-sm-6 form-group">
                                        <label> نام*</label>
                                        <input type="text" placeholder="" class="form-control" name="name"
                                               data-error="این فیلد ضروری است" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="col-sm-6 form-group">
                                        <label>ایمیل*</label>
                                        <input type="email" placeholder="" class="form-control" name="email"
                                               data-error="این فیلد ضروری است" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="col-12 form-group mb-0">
                                        <button type="submit" class="item-btn">ارسال نظر</button>
                                    </div>
                                </div>
                                <div class="form-response"></div>
                            </form>
                        </div>
                        <div class="related-listing">
                            <h2 class="related-title">شاید شما هم دوست داشته باشید</h2>
                            <div id="owl-nav1" class="nav-control-layout3">
                                    <span class="rt-prev">
                                        <i class="fas fa-chevron-right"></i>
                                    </span>
                                <span class="rt-next">
                                        <i class="fas fa-chevron-left"></i>
                                    </span>
                            </div>
                        </div>
                        <div class="rc-carousel listing-box-grid" data-custom-nav="#owl-nav1" data-loop="true"
                             data-items="4" data-margin="30" data-autoplay="false" data-autoplay-timeout="5000"
                             data-smart-speed="2000" data-dots="false" data-nav="false" data-nav-speed="false"
                             data-r-x-small="1" data-r-x-small-nav="false" data-r-x-small-dots="false"
                             data-r-x-medium="1" data-r-x-medium-nav="false" data-r-x-medium-dots="false"
                             data-r-small="2" data-r-small-nav="false" data-r-small-dots="false" data-r-medium="2"
                             data-r-medium-nav="false" data-r-medium-dots="false" data-r-large="2" data-r-large-nav="false"
                             data-r-large-dots="false" data-r-extra-large="2" data-r-extra-large-nav="false"
                             data-r-extra-large-dots="false">
                            <div class="product-box">
                                <div class="item-img bg--gradient-50">
                                    <div class="item-status status-open active">باز</div>
                                    <div class="item-status status-save">صرفه جویی 15٪</div>
                                    <img src="{{asset('template/img/listing/listing-1-1.jpg')}}" alt="Listing" class="img-fluid grid-view-img">
                                    <img src="{{asset('template/img/listing/listing-1-2.jpg')}}" alt="Listing" class="img-fluid list-view-img">
                                    <ul class="item-rating">
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><span>8.4<span> / 10</span></span> </li>
                                    </ul>
                                    <div class="item-logo"><img src="{{asset('template/img/listing/logo1.png')}}" alt="Logo"></div>
                                </div>
                                <div class="item-content">
                                    <h3 class="item-title"><a href="#">رستوران وستفیلد</a></h3>
                                    <p class="item-paragraph">لورم ایپسوم متن ساختگی با تولید سادگی ...
                                    </p>
                                    <ul class="contact-info">
                                        <li><i class="fas fa-map-marker-alt"></i>ایران، تهران، تهران، ساختمان تبانی</li>
                                        <li><i class="flaticon-phone-call"></i>+ 132 899 6330</li>
                                        <li><i class="fas fa-globe"></i>www.restauant.com</li>
                                    </ul>
                                    <ul class="meta-item">
                                        <li class="item-btn"><a href="#" class="btn-fill">جزئیات</a></li>
                                        <li class="ctg-name"><a href="#"><i class="flaticon-chef"></i>رستوران</a></li>
                                        <li class="entry-meta">
                                            <ul>
                                                <li class="tooltip-item ctg-icon" data-tips="رستوران"><a href="#"><i
                                                            class="flaticon-chef"></i></a></li>
                                                <li class="tooltip-item" data-tips="مورد علاقه من"><a href="#"><i
                                                            class="fas fa-heart"></i></a></li>
                                                <li class="tooltip-item" data-tips="گالری"><a href="#"><i class="far fa-image"></i></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-4 sidebar-widget-area sidebar-break-md">
                    <div class="widget widget-claim-box widget-box-padding">
                        <h4 class="item-title">آیا این به شما مربوط است؟</h4>
                        <p>آگهی های ما بهترین راه برای مدیریت و محافظت از کسب و کار شماست.</p>
                        <a href="#" class="item-btn">دریافتش کنید</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Listing Area End Here -->


@endsection
