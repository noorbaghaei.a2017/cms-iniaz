
@extends('template.app')

@section('content')


  <!-- content-->
  <div class="content">
                    <!--  section  -->
                    <section class="parallax-section single-par" data-scrollax-parent="true">
                        <div class="bg par-elem "  data-bg="{{asset('template/images/bg/11.jpg')}}" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="overlay op7"></div>
                        <div class="container">
                            <div class="section-title center-align big-title">
                                <h2><span> {{__('cms.contact_us')}} </span></h2>
                                <span class="section-separator"></span>
                            </div>
                        </div>
                        <div class="header-sec-link">
                            <a href="#sec1" class="custom-scroll-link"><i class="fal fa-angle-double-down"></i></a> 
                        </div>
                    </section>
                    <!--  section  end-->               
                    <!--  section  -->
                    <section   id="sec1" data-scrollax-parent="true">
                        <div class="container">
                            <!--about-wrap -->
                            <div class="about-wrap">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="ab_text-title fl-wrap">
                                        
                                            <span class="section-separator fl-sec-sep"></span>
                                        </div>
                                        <!--box-widget-item -->                                       
                                        <div class="box-widget-item fl-wrap block_box">
                                            <div class="box-widget">
                                                <div class="box-widget-content bwc-nopad">
                                                    <div class="list-author-widget-contacts list-item-widget-contacts bwc-padside">
                                                        <ul class="no-list-style">
                                                            <li><span><i class="fal fa-map-marker"></i> {{__('cms.address')}} :</span> <a href="#singleMap" class="custom-scroll-link">ایران , تهران , خیابان آزادی</a></li>
                                                            <li><span><i class="fal fa-phone"></i> {{__('cms.mobile')}} :</span> <a href="#">021-12345678</a></li>
                                                            <li><span><i class="fal fa-envelope"></i> {{__('cms.email')}} :</span> <a href="#">AlisaNoory@domain.com</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="list-widget-social bottom-bcw-box  fl-wrap">
                                                        <ul class="no-list-style">
                                                            <li><a href="#" target="_blank" ><i class="fab fa-facebook-f"></i></a></li>
                                                            <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                                            <li><a href="#" target="_blank" ><i class="fab fa-vk"></i></a></li>
                                                            <li><a href="#" target="_blank" ><i class="fab fa-instagram"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--box-widget-item end -->  
                                        <!--box-widget-item -->
                                        <div class="box-widget-item fl-wrap" style="margin-top:20px;">
                                            <div class="banner-wdget fl-wrap">
                                                <div class="overlay op4"></div>
                                                <div class="bg"  data-bg="{{asset('template/images/bg/18.jpg')}}"></div>
                                                <div class="banner-wdget-content fl-wrap">
                                                    <a href="#" class="color-bg">{{__('cms.read_more')}} </a>
                                                </div>
                                            </div>
                                        </div>
                                        <!--box-widget-item end -->                                            
                                    </div>
                                    <div class="col-md-8">
                                        <div class="ab_text">
                               
                                            <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و متخصصان را می طلبد.</p>
                                            <div id="contact-form">
                                                <div id="message"></div>
                                                <form  class="custom-form" action="php/contact.php" name="contactform" id="contactform">
                                                    <fieldset>
                                                        <label><i class="fal fa-user"></i></label>
                                                        <input type="text" name="name" id="name" placeholder=" {{__('cms.fulle_name')}} *" value=""/>
                                                        <div class="clearfix"></div>
                                                        <label><i class="fal fa-envelope"></i>  </label>
                                                        <input type="text"  name="email" id="email" placeholder="{{__('cms.email')}} *" value=""/>
                                                        <textarea name="comments"  id="comments" cols="40" rows="3" placeholder=" {{__('cms.message')}}:"></textarea>
                                                    </fieldset>
                                                    <button class="btn float-btn color2-bg" id="submit">{{__('cms.send')}} <i class="fal fa-paper-plane"></i></button>
                                                </form>
                                            </div>
                                            <!-- contact form  end--> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- about-wrap end  --> 
                        </div>
                    </section>
                    <section class="no-padding-section">
                        <div class="map-container">
                            <div id="singleMap" data-latitude="35.725705" data-longitude="51.408263" data-mapTitle="Our Location"></div>
                        </div>
                    </section>
                </div>
                <!--content end-->





@endsection

