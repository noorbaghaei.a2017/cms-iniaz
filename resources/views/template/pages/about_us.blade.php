@extends('template.app')

@section('content')


 <!-- content-->
 <div class="content">
                    <div class="page-scroll-nav">
                        <nav class="scroll-init color2-bg">
                            <ul class="no-list-style">
                                <li><a class="act-scrlink tolt" href="#sec1" data-microtip-position="left" data-tooltip=" {{__('cms.about-us')}}"><i class="fal fa-building"></i></a></li>
                                <li><a href="#sec2" class="tolt" data-microtip-position="left" data-tooltip="فیلم تبلیغاتی"><i class="fal fa-video"></i></a></li>
                                <li><a href="#sec3" class="tolt" data-microtip-position="left" data-tooltip=" {{__('cms.staff')}}"><i class="far fa-users-crown"></i></a></li>
                                <li><a href="#sec4" class="tolt" data-microtip-position="left" data-tooltip="چرا ما"><i class="fal fa-user-astronaut"></i></a></li>
                                <li><a href="#sec5" class="tolt" data-microtip-position="left" data-tooltip="{{__('cms.comment')}} "><i class="fal fa-comment-alt-smile"></i></a></li>
                            </ul>
                        </nav>
                    </div>
                    <!--  section  -->
                    <section class="parallax-section single-par" data-scrollax-parent="true">
                        <div class="bg par-elem "  data-bg="{{asset('template/images/bg/30.jpg')}}" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="overlay op7"></div>
                        <div class="container">
                            <div class="section-title center-align big-title">
                                <h2><span>  {{__('cms.about-us')}}</span></h2>
                            </div>
                        </div>
                        <div class="header-sec-link">
                            <a href="#sec1" class="custom-scroll-link"><i class="fal fa-angle-double-down"></i></a> 
                        </div>
                    </section>
                    <!--  section  end-->
                    <section   id="sec1" data-scrollax-parent="true">
                        <div class="container">
                         
                            <!--about-wrap -->
                            <div class="about-wrap">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="list-single-main-media fl-wrap" style="box-shadow: 0 9px 26px rgba(58, 87, 135, 0.2);">
                                            <img src="{{asset('template/images/all/55.jpg')}}" class="respimg" alt="">
											<a href="#" class="promo-link   image-popup"><i class="fal fa-video"></i><span> {{__('cms.about-us')}}</span></a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="ab_text">
                                            <div class="ab_text-title fl-wrap">
                                                <h3>داستان تیم <span>عالی ما</span></h3>
                                                <h4>برای اطلاع بیشتر درباره ما ، ویدیو را بررسی کنید .</h4>
                                                <span class="section-separator fl-sec-sep"></span>
                                            </div>
                                            <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. </p>
                                            <p>
                                                لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد.
                                            </p>
                                            <a href="#sec3" class="btn color2-bg float-btn custom-scroll-link"> {{__('cms.staff')}} <i class="fal fa-users"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- about-wrap end  --> 
                            <span class="fw-separator"></span>
                            <div class=" single-facts bold-facts fl-wrap">
                                <!-- inline-facts -->
                                <div class="inline-facts-wrap">
                                    <div class="inline-facts">
                                        <div class="milestone-counter">
                                            <div class="stats animaper">
                                                <div class="num" data-content="0" data-num="1254">1254</div>
                                            </div>
                                        </div>
                                        <h6>بازدید کنندگان جدید هر هفته</h6>
                                    </div>
                                </div>
                                <!-- inline-facts end -->
                                <!-- inline-facts  -->
                                <div class="inline-facts-wrap">
                                    <div class="inline-facts">
                                        <div class="milestone-counter">
                                            <div class="stats animaper">
                                                <div class="num" data-content="0" data-num="12168">12168</div>
                                            </div>
                                        </div>
                                        <h6>مشتریان راضی هر ساله</h6>
                                    </div>
                                </div>
                                <!-- inline-facts end -->
                                <!-- inline-facts  -->
                                <div class="inline-facts-wrap">
                                    <div class="inline-facts">
                                        <div class="milestone-counter">
                                            <div class="stats animaper">
                                                <div class="num" data-content="0" data-num="2172">2172</div>
                                            </div>
                                        </div>
                                        <h6>جوایز برنده شد</h6>
                                    </div>
                                </div>
                                <!-- inline-facts end -->
                                <!-- inline-facts  -->
                                <div class="inline-facts-wrap">
                                    <div class="inline-facts">
                                        <div class="milestone-counter">
                                            <div class="stats animaper">
                                                <div class="num" data-content="0" data-num="732">732</div>
                                            </div>
                                        </div>
                                        <h6>لیست های جدید هر هفته</h6>
                                    </div>
                                </div>
                                <!-- inline-facts end -->
                            </div>
                        </div>
                    </section>
                    <!--section end-->  
                    <!--section  -->  
                    <section class="gray-bg particles-wrapper">
                        <div class="container">
                            <div class="section-title">
                                <h2>  {{__('cms.feeling')}}</h2>
                                <div class="section-subtitle"> {{__('cms.feeling')}}  </div>
                                <span class="section-separator"></span>
                            </div>
                            <div class="process-wrap_time-line fl-wrap">
                                <!--process-item-->
                                <div class="process-item_time-line">
                                    <div class="pi_head color-bg">1</div>
                                    <div class="pi-text fl-wrap">
                                        <h4>تدوین یک استراتژی مؤثر</h4>
                                        <p>چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.</p>
                                    </div>
                                </div>
                                <!--process-item end-->
                                <!--process-item-->
                                <div class="process-item_time-line">
                                    <div class="pi_head color-bg">2</div>
                                    <div class="pi-text fl-wrap">
                                        <h4>توسعه و ادغام وب سایت</h4>
                                        <p>چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.</p>
                                    </div>
                                </div>
                                <!--process-item end-->
                                <!--process-item-->
                                <div class="process-item_time-line">
                                    <div class="pi_head color-bg">3</div>
                                    <div class="pi-text fl-wrap">
                                        <h4>تست و پشتیبانی حرفه ای</h4>
                                        <p>چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.</p>
                                    </div>
                                </div>
                                <!--process-item end-->                                                            
                            </div>
                            <a href="#" class="btn color2-bg">{{__('cms.read_more')}}  <i class="fal fa-angle-left"></i></a>
                        </div>
                        <div id="particles-js" class="particles-js"></div>
                    </section>
                    <!--section end-->  
                    <!--section  -->  
                    <section class="parallax-section video-section" data-scrollax-parent="true" id="sec2">
                        <div class="bg par-elem "  data-bg="{{asset('template/images/bg/34.jpg')}}" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="overlay op7"></div>
                        <!--container-->
                        <div class="container">
                            <div class="video_section-title fl-wrap">
                                <h4>تولید کننده لورم ایپسوم</h4>
                                <h2>برای شروع سفر هیجان انگیز خود آماده شوید. <br> آژانس ما شما را در دنیای شگفت انگیز دیجیتال هدایت می کند</h2>
                            </div>
                            <a href="https://vimeo.com/7062" class="promo-link big_prom   image-popup"><i class="fal fa-play"></i><span>فیلم تبلیغاتی</span></a>
                        </div>
                    </section>
                    <!--section end-->  
                    <!--section -->  
                    <section id="sec3">
                        <div class="container">
                            <div class="section-title">
                                <h2> {{__('cms.staff')}} </h2>
                                <div class="section-subtitle">{{__('cms.staff')}}</div>
                             
                            </div>
                            <div class="about-wrap team-box2 fl-wrap">
                                <!-- team-item -->
                                <div class="team-box">
                                    <div class="team-photo">
                                        <img src="{{asset('template/images/team/2.jpg')}}" alt="" class="respimg">
                                    </div>
                                    <div class="team-info fl-wrap">
                                        <h3><a href="#"> تیلور رابرتز</a></h3>
                                        <h4>مشاور کسب و کار</h4>
                                        <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است.  </p>
                                        <div class="team-social">
                                            <ul class="no-list-style">
                                                <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fab fa-vk"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- team-item  end-->
                               
                            </div>
                        </div>
                        <div class="waveWrapper waveAnimation">
                          <div class="waveWrapperInner bgMiddle">
                            <div class="wave-bg-anim waveMiddle" style="background-image: url({{asset('template/images/wave-top.png')}})"></div>
                          </div>
                          <div class="waveWrapperInner bgBottom">
                            <div class="wave-bg-anim waveBottom" style="background-image: url({{asset('template/images/wave-top.png')}})"></div>
                          </div>
                        </div> 						
                    </section>
                    <!--section end-->  
                    <!--section  -->  
                    <section class="gray-bg">
                        <div class="container">
                            <div class="clients-carousel-wrap fl-wrap">
                                <div class="cc-btn   cc-prev"><i class="fal fa-angle-left"></i></div>
                                <div class="cc-btn cc-next"><i class="fal fa-angle-right"></i></div>
                                <div class="clients-carousel">
                                    <div class="swiper-container">
                                        <div class="swiper-wrapper">
                                            <!--client-item-->
                                            <div class="swiper-slide">
                                                <a href="#" class="client-item"><img src="{{asset('template/images/clients/1.png')}}" alt=""></a>
                                            </div>
                                            <!--client-item end-->
                                            <!--client-item-->
                                            <div class="swiper-slide">
                                                <a href="#" class="client-item"><img src="{{asset('template/images/clients/2.png')}}" alt=""></a>
                                            </div>
                                            <!--client-item end-->
                                            <!--client-item-->
                                            <div class="swiper-slide">
                                                <a href="#" class="client-item"><img src="{{asset('template/images/clients/3.png')}}" alt=""></a>
                                            </div>
                                            <!--client-item end-->
                                            <!--client-item-->
                                            <div class="swiper-slide">
                                                <a href="#" class="client-item"><img src="{{asset('template/images/clients/1.png')}}" alt=""></a>
                                            </div>
                                            <!--client-item end-->
                                            <!--client-item-->
                                            <div class="swiper-slide">
                                                <a href="#" class="client-item"><img src="{{asset('template/images/clients/2.png')}}" alt=""></a>
                                            </div>
                                            <!--client-item end-->
                                            <!--client-item-->
                                            <div class="swiper-slide">
                                                <a href="#" class="client-item"><img src="{{asset('template/images/clients/3.png')}}" alt=""></a>
                                            </div>
                                            <!--client-item end-->                                                                                                                                                                                                                                                
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--section end-->  
                    <!--section  -->  
                    <section class="parallax-section" data-scrollax-parent="true" id="sec4">
                        <div class="bg par-elem "  data-bg="{{asset('template/images/bg/33.jpg')}}" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="overlay op7"></div>
                        <!--container-->
                        <div class="container">
                            <div class="section-title center-align big-title">
                                <h2><span>  {{__('cms.features')}}</span></h2>
                                <span class="section-separator"></span>
                            </div>
                        </div>
                    </section>
                    <!--section end-->  
                    <!--section  -->  
                    <section class="gray-bg absolute-wrap_section">
                        <div class="container">
                            <div class="absolute-wrap fl-wrap">
                                <!-- features-box-container --> 
                                <div class="features-box-container fl-wrap">
                                    <div class="row">
                                        <!--features-box --> 
                                        <div class="col-md-4">
                                            <div class="features-box">
                                                <div class="time-line-icon">
                                                    <i class="fal fa-headset"></i>
                                                </div>
                                                <h3>پشتیبانی 24 ساعته</h3>
                                                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه.    </p>
                                            </div>
                                        </div>
                                        <!-- features-box end  --> 
                                        <!--features-box --> 
                                        <div class="col-md-4">
                                            <div class="features-box gray-bg">
                                                <div class="time-line-icon">
                                                    <i class="fal fa-users-cog"></i>
                                                </div>
                                                <h3>پنل مدیریت</h3>
                                                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه.   </p>
                                            </div>
                                        </div>
                                        <!-- features-box end  --> 
                                        <!--features-box --> 
                                        <div class="col-md-4">
                                            <div class="features-box ">
                                                <div class="time-line-icon">
                                                    <i class="fal fa-mobile"></i>
                                                </div>
                                                <h3>دوستانه</h3>
                                                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه.  </p>
                                            </div>
                                        </div>
                                        <!-- features-box end  -->  
                                    </div>
                                </div>
                                <!-- features-box-container end  -->                             
                            </div>
                            <div class="section-separator"></div>
                        </div>
                    </section>
                    <!--section end-->  
                    <!--section  -->  
                    <section id="sec5">
                        <div class="container">
                            <div class="section-title">
                                <h2>  {{__('cms.comment')}}</h2>
                                <div class="section-subtitle">{{__('cms.comment')}} </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="testimonilas-carousel-wrap fl-wrap">
                            <div class="listing-carousel-button listing-carousel-button-next"><i class="fas fa-caret-right"></i></div>
                            <div class="listing-carousel-button listing-carousel-button-prev"><i class="fas fa-caret-left"></i></div>
                            <div class="testimonilas-carousel">
                                <div class="swiper-container">
                                    <div class="swiper-wrapper">
                                        <!--testi-item-->
                                        <div class="swiper-slide">
                                            <div class="testi-item fl-wrap">
                                                <div class="testi-avatar"><img src="{{asset('template/images/avatar/4.jpg')}}" alt=""></div>
                                                <div class="testimonilas-text fl-wrap">
                                                    <div class="listing-rating card-popup-rainingvis" data-starrating2="5"></div>
                                                    <p>"لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته. "</p>
                                                    <a href="#" class="testi-link" target="_blank">از طریق فیسبوک</a>
                                                    <div class="testimonilas-avatar fl-wrap">
                                                        <h3>سنتا سیمپسون</h3>
                                                        <h4>صاحب رستوران</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--testi-item end-->
                                        <!--testi-item-->
                                        <div class="swiper-slide">
                                            <div class="testi-item fl-wrap">
                                                <div class="testi-avatar"><img src="{{asset('template/images/avatar/4.jpg')}}" alt=""></div>
                                                <div class="testimonilas-text fl-wrap">
                                                    <div class="listing-rating card-popup-rainingvis" data-starrating2="5"></div>
                                                    <p>"لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته. "</p>
                                                    <a href="#" class="testi-link" target="_blank">از طریق فیسبوک</a>
                                                    <div class="testimonilas-avatar fl-wrap">
                                                        <h3>سنتا سیمپسون</h3>
                                                        <h4>صاحب رستوران</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--testi-item end-->
                                        <!--testi-item-->
                                        <div class="swiper-slide">
                                            <div class="testi-item fl-wrap">
                                                <div class="testi-avatar"><img src="{{asset('template/images/avatar/4.jpg')}}" alt=""></div>
                                                <div class="testimonilas-text fl-wrap">
                                                    <div class="listing-rating card-popup-rainingvis" data-starrating2="5"></div>
                                                    <p>"لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته. "</p>
                                                    <a href="#" class="testi-link" target="_blank">از طریق فیسبوک</a>
                                                    <div class="testimonilas-avatar fl-wrap">
                                                        <h3>سنتا سیمپسون</h3>
                                                        <h4>صاحب رستوران</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--testi-item end-->
                                        <!--testi-item-->
                                        <div class="swiper-slide">
                                            <div class="testi-item fl-wrap">
                                                <div class="testi-avatar"><img src="{{asset('template/images/avatar/4.jpg')}}" alt=""></div>
                                                <div class="testimonilas-text fl-wrap">
                                                    <div class="listing-rating card-popup-rainingvis" data-starrating2="5"></div>
                                                    <p>"لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته. "</p>
                                                    <a href="#" class="testi-link" target="_blank">از طریق فیسبوک</a>
                                                    <div class="testimonilas-avatar fl-wrap">
                                                        <h3>سنتا سیمپسون</h3>
                                                        <h4>صاحب رستوران</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--testi-item end-->
                                        <!--testi-item-->
                                        <div class="swiper-slide">
                                            <div class="testi-item fl-wrap">
                                                <div class="testi-avatar"><img src="{{asset('template/images/avatar/4.jpg')}}" alt=""></div>
                                                <div class="testimonilas-text fl-wrap">
                                                    <div class="listing-rating card-popup-rainingvis" data-starrating2="5"></div>
                                                    <p>"لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته. "</p>
                                                    <a href="#" class="testi-link" target="_blank">از طریق فیسبوک</a>
                                                    <div class="testimonilas-avatar fl-wrap">
                                                        <h3>سنتا سیمپسون</h3>
                                                        <h4>صاحب رستوران</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--testi-item end-->                                                          
                                    </div>
                                </div>
                            </div>
                            <div class="tc-pagination"></div>
                        </div>
                    </section>
                    <!--section end-->  
                </div>
                <!--content end-->




@endsection
