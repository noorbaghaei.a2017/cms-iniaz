</div>
            <!-- wrapper end-->
            <!--footer -->
            <footer class="main-footer fl-wrap">
                <!-- footer-header-->
                <div class="footer-header fl-wrap grad ient-dark">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-5">
                                <div  class="subscribe-header">
                                    <h3>{{__('cms.subscribe-newsletter')}}</h3>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="subscribe-widget">
                                    <div class="subcribe-form">
                                        <form id="subscribe">
                                            <input class="enteremail fl-wrap" name="email" id="subscribe-email" placeholder="{{__('cms.enter-your-email')}}" spellcheck="false" type="text">
                                            <button type="submit" id="subscribe-button" class="subscribe-button"><i class="fal fa-envelope"></i></button>
                                            <label for="subscribe-email" class="subscribe-message"></label>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer-header end-->
                <!--footer-inner-->
                <div class="footer-inner   fl-wrap">
                    <div class="container">
                        <div class="row">

                        @if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")
                               
                                 <!-- footer-widget-->
                            <div class="col-md-4">
                                <div class="footer-widget fl-wrap">
                                    <div class="footer-logo"><a href="{{route('front.website')}}"><img src="{{asset('template/images/logo.png')}}" alt=""></a></div>
                                    <div class="footer-contacts-widget fl-wrap">
                                        <p> {{$setting->about}}  </p>
                                        <ul  class="footer-contacts fl-wrap no-list-style">
                                            <li><span><i class="fal fa-envelope"></i> {{__('cms.email')}} :</span><a href="#" target="_blank">yourmail@domain.com</a></li>
                                            <li> <span><i class="fal fa-map-marker"></i> {{__('cms.address')}} :</span><a href="#" target="_blank">ایران , تهران , خیابان آزادی</a></li>
                                            <li><span><i class="fal fa-phone"></i> {{__('cms.phone')}} :</span><a href="#">02198765432</a></li>
                                        </ul>
                                        <div class="footer-social">
                                            <span>{{__('cms.follow_us')}}: </span>
                                            <ul class="no-list-style">
                                                <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fab fa-vk"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fab fa-whatsapp"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- footer-widget end-->
                            <!-- footer-widget-->
                            <div class="col-md-4">
                                <div class="footer-widget fl-wrap">
                                    <h3>{{__('cms.last_article')}}  </h3>
                                    <div class="footer-widget-posts fl-wrap">
                                        <ul class="no-list-style">
                                            <li class="clearfix">
                                                <a href="#"  class="widget-posts-img"><img src="{{asset('template/images/all/4.jpg')}}" class="respimg" alt=""></a>
                                                <div class="widget-posts-descr">
                                                    <a href="#" title="">لورم ایپسوم متن ساختگی با تولید</a>
                                                    <span class="widget-posts-date"><i class="fal fa-calendar"></i> 21 اسفند 09.05 </span> 
                                                </div>
                                            </li>
                                           
                                           
                                        </ul>
                                        <a href="blog.html" class="footer-link">{{__('cms.all')}}  <i class="fal fa-long-arrow-left"></i></a>
                                    </div>
                                </div>
                            </div>
                            <!-- footer-widget end-->
                            <!-- footer-widget  -->
                            <div class="col-md-4">
                                <div class="footer-widget fl-wrap ">
                                   
                                    <div class="twitter-holder fl-wrap scrollbar-inner2" data-simplebar data-simplebar-auto-hide="false">
                                        <div id="footer-twiit"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- footer-widget end-->

                               
                               
                               

                        @else
                          

                            <!-- footer-widget  -->
                            <div class="col-md-4">
                                <div class="footer-widget fl-wrap ">
                                    <h3> </h3>
                                    <div class="twitter-holder fl-wrap scrollbar-inner2" data-simplebar data-simplebar-auto-hide="false">
                                        <div id="footer-twiit"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- footer-widget end-->

  <!-- footer-widget-->
  <div class="col-md-4">
                                <div class="footer-widget fl-wrap">
                                    <h3> {{__('cms.last_article')}}</h3>
                                    <div class="footer-widget-posts fl-wrap">
                                        <ul class="no-list-style">
                                            <li class="clearfix">
                                                
                                                <a href="#"  class="widget-posts-img"><img src="{{asset('template/images/all/4.jpg')}}" class="respimg" alt=""></a>
                                                <div class="widget-posts-descr">
                                                    <a href="#" title="">لورم ایپسوم متن ساختگی با تولید</a>
                                                    <span class="widget-posts-date"><i class="fal fa-calendar"></i> 21 اسفند 09.05 </span> 
                                                </div>
                                            </li>
                                         
                                           
                                        </ul>
                                        <a href="blog.html" class="footer-link">{{__('cms.all')}}  <i class="fal fa-long-arrow-left"></i></a>
                                    </div>
                                </div>
                            </div>
                            <!-- footer-widget end-->



                            <!-- footer-widget-->
                            <div class="col-md-4">
                                <div class="footer-widget fl-wrap">
                                    <div class="footer-logo"><a href="{{route('front.website')}}"><img src="{{asset('template/images/logo.png')}}" alt=""></a></div>
                                    <div class="footer-contacts-widget fl-wrap">
                                        <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است.   </p>
                                        <ul  class="footer-contacts fl-wrap no-list-style">
                                            <li><span><i class="fal fa-envelope"></i> {{__('cms.email')}} :</span><a href="#" target="_blank">yourmail@domain.com</a></li>
                                            <li> <span><i class="fal fa-map-marker"></i> {{__('cms.address')}} :</span><a href="#" target="_blank">ایران , تهران , خیابان آزادی</a></li>
                                            <li><span><i class="fal fa-phone"></i> {{__('cms.phone')}} :</span><a href="#">02198765432</a></li>
                                        </ul>
                                        <div class="footer-social">
                                            <span>{{__('cms.follow_us')}}: </span>
                                            <ul class="no-list-style">
                                                <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fab fa-vk"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fab fa-whatsapp"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- footer-widget end-->
                          
                          




                         @endif   
                        </div>
                    </div>
                    <!-- footer bg-->
                    <div class="footer-bg" data-ran="4"></div>
                    <div class="footer-wave">
                        <svg viewbox="0 0 100 25">
                            <path fill="#fff" d="M0 30 V12 Q30 6 55 12 T100 11 V30z" />
                        </svg>
                    </div>
                    <!-- footer bg  end-->
                </div>
                <!--footer-inner end -->
                <!--sub-footer-->
                <div class="sub-footer  fl-wrap">
                    <div class="container">
                        <div class="copyright"> می باشد iniaz کلیه حقوق محفوظ است.</div>
                       
                        <div class="subfooter-nav">
                            <ul class="no-list-style">
                                <li><a href="#">شرایط استفاده</a></li>
                                <li><a href="#">سیاست حفظ حریم خصوصی</a></li>
                                <li><a href="#">وبلاگ</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--sub-footer end -->
            </footer>
            <!--footer end -->  
            <!--map-modal -->
            <div class="map-modal-wrap">
                <div class="map-modal-wrap-overlay"></div>
                <div class="map-modal-item">
                    <div class="map-modal-container fl-wrap">
                        <div class="map-modal fl-wrap">
                            <div id="singleMap" data-latitude="40.7" data-longitude="-73.1"></div>
                        </div>
                        <h3><span>مکان برای : </span><a href="#">عنوان لیست</a></h3>
                        <div class="map-modal-close"><i class="fal fa-times"></i></div>
                    </div>
                </div>
            </div>
            <!--map-modal end -->                
            <!--register form -->
            <div class="main-register-wrap modal">
                <div class="reg-overlay"></div>
                <div class="main-register-holder tabs-act">
                    <div class="main-register fl-wrap  modal_main">
                        <div class="main-register_title"> <span>  iniaz</span></div>
                        <div class="close-reg"><i class="fal fa-times"></i></div>
                        <div>
                        <p id="result-ajax"> </p>

                            </div>
                        <ul class="tabs-menu fl-wrap no-list-style">
                            <li class="current"><a href="#tab-1"><i class="fal fa-sign-in-alt"></i> {{__('cms.login')}}</a></li>
                            <li><a href="#tab-2"><i class="fal fa-user-plus"></i>  {{__('cms.register')}}</a></li>
                            <li><a href="#tab-3"><i class="fal fa-key"></i>  {{__('cms.forget_password')}}</a></li>

                        </ul>
                        <!--tabs -->                       
                        <div class="tabs-container">
                            <div class="tab">
                                <!--tab -->
                                <div id="tab-1" class="tab-content first-tab">
                                    <div class="custom-form">
                                        
                                            <label>{{__('cms.username')}}   <span>*</span> </label>
                                            <input name="email" type="text" id="email"   onClick="this.select()" value="">
                                            <label > {{__('cms.password')}} <span>*</span> </label>
                                            <input name="password" type="password" id="password"    onClick="this.select()" value="" >
                                            <button id="btn-login"  onclick="Login()"  class="btn float-btn color2-bg"> {{__('cms.login')}} <i class="fas fa-caret-left"></i></button>
                                            <div class="clearfix"></div>
                                            
                                    
                                        <!-- <div class="lost_password">
                                            <a href="#">رمزعبور خود را گم کردید؟</a>
                                        </div> -->
                                    </div>
                                </div>
                                <!--tab end -->
                                <!--tab -->
                                <div class="tab">
                                    <div id="tab-2" class="tab-content">
                                        <div class="custom-form">
                                            
                                            <label > {{__('cms.first_name')}}  <span>*</span> </label>
                                            <input  id="first_name_r" type="text"   onClick="this.select()" value="">
                                                <label > {{__('cms.last_name')}}  <span>*</span> </label>
                                                <input  id="last_name_r" type="text"   onClick="this.select()" value="">
                                                <label>{{__('cms.email')}}  <span>*</span></label>
                                                <input  id="email_r" type="text"  onClick="this.select()" value="">
                                                <label > {{__('cms.password')}} <span>*</span></label>
                                                <input  id="password_r" type="password"   onClick="this.select()" value="" >
                                               
                                                <div class="clearfix"></div>
                                                <button id="btn-register"   onclick="Register()"    class="btn float-btn color2-bg">  {{__('cms.register')}}  <i class="fas fa-caret-left"></i></button>
                                           
                                        </div>
                                    </div>
                                </div>
                                <!--tab end -->

                            <!--tab -->
                            <div class="tab">
                                    <div id="tab-3" class="tab-content">
                                    <span id="code_hidden" data-client="" style="display:none"></span>
                                        <div class="custom-form" >
                                            <div class="box-email">
                                            <label > {{__('cms.email')}}  <span>*</span> </label>
                                            <input  id="email_f" type="text"   onClick="this.select()" value="">
                                        </div>
                                                <div class="clearfix"></div>
                                                <button id="btn-sendCode"   onclick="sendCode()"    class="btn float-btn color2-bg">  {{__('cms.send-code')}}  <i class="fas fa-caret-left"></i></button>
                                           
                                        </div>
                                    </div>
                                </div>
                                <!--tab end -->

                            </div>
                            <!--tabs end -->
                           
                            
                         
                        </div>
                    </div>
                </div>
            </div>
            <!--register form end -->
            <a class="to-top"><i class="fas fa-caret-up"></i></a>     
        </div>
        <!-- Main end -->
        <!--=============== scripts  ===============-->
        <script src="{{asset('template/js/jquery.min.js')}}"></script>
        <script src="{{asset('template/js/plugins.js')}}"></script>
        <script src="{{asset('template/js/scripts.js')}}"></script>

        @yield('scripts')
        <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_8C7p0Ws2gUu7wo0b6pK9Qu7LuzX2iWY&libraries=places&callback=initAutocomplete"></script> -->
        <!-- <script src="{{asset('template/js/map-single.js')}}"></script> -->
        
        <script>

            function Login() {

        msg_error="{{__('cms.invalid-data')}}";
        msg_error_server="{{__('cms.msg_error_server')}}";

            $user=$('input#email').val( );
            $pass=$('input#password').val( );
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type:'POST',
                contentType:'application/json; charset=utf-8',
                url:'/ajax/login/'+$user+'/'+$pass,
                data: { field1:$user,field2:$pass} ,
                beforeSend:function(){
                    $('#btn-login').hide();
                },
                success: function (response) {
                    if(response.data.result){
                    
                        window.location.href = '/user/panel/dashboard'
                    }
                    else {
                        $("#result-ajax").empty()
                            .append("<span>"+msg_error+"</span>");
                    }

                },
                error: function (xhr,ajaxOptions,thrownError) {

                    $("#result-ajax").empty()
                            .append("<span>"+msg_error_server+"</span>");
                },
                complete:function(){
                    $('#btn-login').show();
                }
     

            });

            }

            function Register() {

                msg_error_server="{{__('cms.msg_error_server')}}";
            email=$('input#email_r').val( ); 
            $password=$('input#password_r').val( ); 
            $first=$('input#first_name_r').val( ); 
            $last=$('input#last_name_r').val( ); 

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type:'POST',
                contentType:'application/json; charset=utf-8',
                url:'/ajax/register/client/'+email+'/'+$password+"/"+$first+'/'+$last,
                data: {email:email} ,
                beforeSend:function(){
                    $('#btn-register').hide();
                },
                success: function (response) {
                    if(response.data.result){
                      
                      
                
                        window.location.href = '/user/panel/dashboard'
                    }
                    else {
                        
                        $("#result-ajax").empty();
                        jQuery.each(response.data.errors, function(index, itemData) {
                            $("#result-ajax").append("<span>"+itemData+"</span><br>");
                                });
                            

                           
                    }

                },
                error: function (xhr,ajaxOptions,thrownError) {
                    
                    $("#result-ajax").empty()
                            .append("<span>"+msg_error_server+"</span>");
                },
                complete:function(){
                    $('#btn-register').show();
                }


            });

            }

            function sendCode() {

                msg_error_server="{{__('cms.msg_error_server')}}";
                invalid_email="{{__('cms.invalid-email')}}";
                msg="{{__('cms.enter-your-verify-code')}}";
                text="{{__('cms.please-check-your-email-for-verify-account-right-now')}}";
                btn="{{__('cms.verify-code')}}";
                $email=$('input#email_f').val( );
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type:'POST',
                    contentType:'application/json; charset=utf-8',
                    url:'/ajax/email/verify/'+$email,
                    data: { field1:$email} ,
                beforeSend:function(){
                    $('#btn-sendCode').hide();
                },
                    success: function (response) {
                    
                        if(response.data.result){

                        $(".box-email").empty()
                        .append("<input id='code_verify' onClick='this.select()' placeholder='"+msg+"' type='text' class='form-control' autocomplete='off'>");
                        $("#result-ajax").empty()
                        .append("<span>"+text+"</span>");
                        $("#code_hidden").attr('data-client',$email)

                        $('#btn-sendCode').html(btn)
                        .attr('onclick','VerifyCode()')
                        }
                        else{
                            $("#result-ajax").empty()
                            .append("<span>"+invalid_email+"</span>");
                        }
                    },
                    error: function (xhr,ajaxOptions,thrownError) {
                        $("#result-ajax").empty()
                            .append("<span>"+msg_error_server+"</span>");
                    },
                    complete:function(){
                    $('#btn-sendCode').show();
                }


                });

                }

        function VerifyCode() {

                msg="{{__('cms.invalid-code')}}";
                $email=$('span#code_hidden').data('client');
                $code=$('input#code_verify').val();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type:'POST',
                    contentType:'application/json; charset=utf-8',
                    url:'/ajax/email/login/verify/'+$email+"/"+$code,
                    data: { field1:$email,field2:$code} ,
                    beforeSend:function(){
                    $('#btn-sendCode').hide();
                },
                    success: function (response) {
                        window.location.href = '/'
                    },
                    error: function (xhr,ajaxOptions,thrownError) {
                    
                       console.log(xhr,ajaxOptions,thrownError);
                        $("#result-ajax").empty()
                            .append("<span>"+msg+"</span>");
                    },
                    complete:function(){
                    $('#btn-sendCode').show();
                }

                });

                }


        </script>

    </body>
</html>