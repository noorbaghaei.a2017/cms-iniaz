
<!DOCTYPE HTML>
<html lang="{{LaravelLocalization::getCurrentLocale()}}">
    <head>
        <!--=============== basic  ===============-->
        <meta charset="UTF-8">
        {!! SEO::generate() !!}

            @yield('seo')
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!--=============== css  ===============-->	
        <link type="text/css" rel="stylesheet" href="{{asset('template/css/reset.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('template/css/plugins.css')}}">
       
        <link type="text/css" rel="stylesheet" href="{{asset('template/css/color.css')}}">

            @if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

            <link type="text/css" rel="stylesheet" href="{{asset('template/css/style.css')}}">
            @else

            <link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-style.css')}}">
            @endif

        <!-- Date Picker CSS -->
        <!-- <link rel="stylesheet" href="{{asset('template/css/date-picker.css')}}"> -->
        <!-- persian picker CSS -->
        <!-- <link rel="stylesheet" href="src/jquery.md.bootstrap.datetimepicker.style.css" /> -->

        <!--=============== favicons ===============-->
          <!-- Favicon -->
    @if(!$setting->Hasmedia('logo'))
        <link rel="shortcut icon" href="{{asset('img/no-img.gif')}}">
        <link rel="apple-touch-icon" href="{{asset('img/no-img.gif')}}">
    @else
        <link rel="shortcut icon" href="{{$setting->getFirstMediaUrl('logo')}}">
        <link rel="apple-touch-icon" href="{{$setting->getFirstMediaUrl('logo')}}">
    @endif
    <!-- Stylesheets -->

    @yield('heads')

    </head>

    <body>
        <!--loader-->
        <div class="loader-wrap">
            <div class="loader-inner">
                <div class="loader-inner-cirle"></div>
            </div>
        </div>
        <!--loader end-->
        <!-- main start  -->
        <div id="main">
            <!-- header -->
            <header class="main-header">
                <!-- logo-->
                <a href="{{route('front.website')}}" class="logo-holder"><img src="{{asset('template/images/logo.png')}}" alt=""></a>
                <!-- logo end-->
              
                <!-- header opt --> 
                @if(auth('client')->check())
                <a href="#" class="add-list color-bg">{{__('cms.post-an-ad')}}  <span><i class="fal fa-layer-plus"></i></span></a>
                @endif
                <div class="cart-btn   show-header-modal" data-microtip-position="bottom" role="tooltip" aria-label="Your Wishlist"><i class="fal fa-heart"></i><span class="cart-counter green-bg"></span> </div>
               @if(auth('client')->check())
               <div class="header-user-menu">
                    <div class="header-user-name">
                        <span>
                        
                        @if(!$client->Hasmedia('images'))
                        <img src="{{asset('template/images/user-icon-2.jpg')}}" alt="">
                        @else

                        <img src="{{$client->getFirstMediaUrl('images')}}" alt="">
                       
                        @endif
                        </span>
                        {{__('cms.hello')}} ، {{$client->first_name}}
                    </div>
                    <ul>
                        <li><a href="{{route('client.edit')}}"> {{__('cms.edit-profile')}}  </a></li>
                        <li><a href="#"> {{__('cms.post-an-ad')}}  </a></li>
                        <li><a href="{{route('client.logout.panel')}}">{{__('cms.logout')}} </a></li>
                    </ul>
                </div>
               @else
               <div class="show-reg-form modal-open avatar-img" data-srcav="{{asset('template/images/avatar/3.jpg')}}"><i class="fal fa-user"></i></div>


               @endif
               
                <!-- header opt end--> 
                <!-- lang-wrap-->
                <div class="lang-wrap">
                    <div class="show-lang"><span><i class="fal fa-globe-europe"></i><strong>{{LaravelLocalization::getCurrentLocale()}}</strong></span><i class="fa fa-caret-down arrlan"></i></div>
                    <ul class="lang-tooltip lang-action no-list-style">
                                 @foreach(config('cms.all-lang') as $language)
                                    @if(LaravelLocalization::getCurrentLocale()!==$language)
                                    <li>
                                    <a  onclick="changeLang({{'$language'}})"  data-lantext="{{$language}}">
                                      {{ $language}}
                                    </a>
                                    </li>
                                    @else
                                   

                                    <li>
                                    <a href="#" class="current-lan" data-lantext="{{$language}}">
                                      {{ $language}}
                                    </a>
                                    </li>
                                    @endif
                                @endforeach
                    
                    </ul>
                </div>
                <!-- lang-wrap end-->                                 
                <!-- nav-button-wrap--> 
                <div class="nav-button-wrap color-bg">
                    <div class="nav-button">
                        <span></span><span></span><span></span>
                    </div>
                </div>
                <!-- nav-button-wrap end-->
                <!--  navigation --> 
                <div class="nav-holder main-menu">
                    <nav>
                        <ul class="no-list-style">

                        @foreach ($top_menus as  $menu)

                        <li><a href="{{$menu->href}}">{!! convert_lang($menu,LaravelLocalization::getCurrentLocale(),'symbol') !!}</a></li>
                        
                        @endforeach
                   
                        </ul>
                    </nav>
                </div>
                <!-- navigation  end -->
     
                <!-- wishlist-wrap--> 
                <div class="header-modal novis_wishlist">
                    <!-- header-modal-container--> 
                    <div class="header-modal-container scrollbar-inner fl-wrap" data-simplebar>
                        <!--widget-posts-->
                        <div class="widget-posts  fl-wrap">
                            <ul class="no-list-style">
                                <li>
                                    <div class="widget-posts-img"><a href="listing-single.html"><img src="{{asset('template/images/gallery/thumbnail/1.png')}}" alt=""></a>  
                                    </div>
                                    <div class="widget-posts-descr">
                                        <h4><a href="listing-single.html">کافه رستوران</a></h4>
                                        <div class="geodir-category-location fl-wrap"><a href="#"><i class="fas fa-map-marker-alt"></i> ایران , تهران , جردن , خیابان بهشت</a></div>
                                        <div class="widget-posts-descr-link"><a href="listing.html" >رستوران ها </a>   <a href="listing.html">کافه</a></div>
                                        <div class="widget-posts-descr-score">4.1</div>
                                        <div class="clear-wishlist"><i class="fal fa-times-circle"></i></div>
                                    </div>
                                </li>
                             
                            </ul>
                        </div>
                        <!-- widget-posts end-->
                    </div>
                    <!-- header-modal-container end--> 
                    <div class="header-modal-top fl-wrap">
                        <h4>لیست دلخواه شما : <span><strong></strong> مکان</span></h4>
                        <div class="close-header-modal"><i class="far fa-times"></i></div>
                    </div>
                </div>
                <!--wishlist-wrap end --> 
            </header>
            <!-- header end-->
            <!-- wrapper-->
            <div id="wrapper">





