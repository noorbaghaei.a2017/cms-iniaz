<!-- start menu item list -->
<ul class="elementskit-navbar-nav nav-alignment-dynamic">
    @foreach($top_menus as $menu)
    <li><a href="{{$menu->href}}">{{convert_lang($menu,LaravelLocalization::getCurrentLocale(),'symbol')}}</a></li>
    @endforeach

</ul> <!-- end menu item list -->
