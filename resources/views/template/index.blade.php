﻿@extends('template.app')

@section('content')

  <!-- content-->
  <div class="content">
                    <!--section  -->
                    <section class="hero-section"   data-scrollax-parent="true">
                        <div class="bg-tabs-wrap">
                            <div class="bg-parallax-wrap" data-scrollax="properties: { translateY: '200px' }">
                                <div class="bg bg_tabs"  data-bg="{{asset('template/images/bg/hero/1.jpg')}}"></div>
                                <div class="overlay op7"></div>
                            </div>
                        </div>
                        <div class="container small-container">
                            <div class="intro-item fl-wrap">
                               
                                <div class="bubbles">
                                    <h1>{{__('cms.find-advertisings')}}</h1>
                                </div>
                            </div>
                            <!-- main-search-input-tabs-->
                            <div class="main-search-input-tabs  tabs-act fl-wrap">
                                <ul class="tabs-menu change_bg no-list-style">
                                    <li class="current"><a href="#tab-inpt1" data-bgtab="{{asset('template/images/bg/hero/1.jpg')}}"> {{__('cms.services')}} </a></li>
                                    <li><a href="#tab-inpt2" data-bgtab="{{asset('template/images/bg/hero/2.jpg')}}"> {{__('cms.products')}}</a></li>
                                </ul>
                                <!--tabs -->                       
                                <div class="tabs-container fl-wrap  ">
                                    <!--tab -->
                                    <div class="tab">
                                        <div id="tab-inpt1" class="tab-content first-tab">
                                            <div class="main-search-input-wrap fl-wrap">
                                                <div class="main-search-input fl-wrap">
                                                    <div class="main-search-input-item">
                                                        <label><i class="fal fa-keyboard"></i></label>
                                                        <input type="text" placeholder=" {{__('cms.looking-for')}} " value=""/>
                                                    </div>
                                                    <div class="main-search-input-item location autocomplete-container">
                                                        <label><i class="fal fa-map-marker-check"></i></label>
                                                        <input type="text" placeholder="{{__('cms.position')}}" class="autocomplete-input" id="autocompleteid" value=""/>
                                                        <a href="#"><i class="fa fa-dot-circle-o"></i></a>
                                                    </div>
                                                    <div class="main-search-input-item">
                                                        <select data-placeholder="All Categories"  class="chosen-select" >
                                                            <option>{{__('cms.all')}}   </option>
                                                            <option>فروشگاه</option>
                                                            <option>هتل</option>
                                                            <option>رستوران</option>
                                                            <option>باشگاه</option>
                                                            <option>کنسرت</option>
                                                        </select>
                                                    </div>
                                                    <button class="main-search-button color2-bg" onclick="window.location.href='#'">{{__('cms.search')}} <i class="far fa-search"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--tab end-->
                                    <!--tab -->
                                    <div class="tab">
                                        <div id="tab-inpt2" class="tab-content">
                                            <div class="main-search-input-wrap fl-wrap">
                                                <div class="main-search-input fl-wrap">
                                                @if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

                                                    <div class="main-search-input-item">
                                                        <label><i class="fal fa-handshake-alt"></i></label>
                                                        <input type="text" placeholder="نام کنسرت یا مکان" value=""/>
                                                    </div>
                                                    <div class="main-search-input-item">
                                                        <select data-placeholder="Loaction" class="chosen-select on-radius" >
                                                            <option>{{__('cms.all')}} </option>
                                                            <option>تهران</option>
                                                            <option>کرج</option>
                                                            <option>اصفهان</option>
                                                            <option>شیراز</option>
                                                            <option>تبریز</option>
                                                            <option>رشت</option>
                                                            <option>همدان</option>
                                                            <option>کرمان</option>
                                                        </select>
                                                    </div>
                                                    <div class="main-search-input-item clact date-container">
                                                        <span class="iconn-dec"><i class="fal fa-calendar"></i></span>
                                                        <input type="text" name="datepicker-here" placeholder="تاریخ کنسرت"    value=""/>
                                                        <span class="clear-singleinput"><i class="fal fa-times"></i></span>
                                                    </div>
                                                    <button class="main-search-button color2-bg" onclick="window.location.href='#'">{{__('cms.search')}} <i class="far fa-search"></i></button>
                                              

                                                    @else
                                                    <button class="main-search-button color2-bg" onclick="window.location.href='#'">{{__('cms.search')}} <i class="far fa-search"></i></button>
                                                    <div class="main-search-input-item clact date-container">
                                                        <span class="iconn-dec"><i class="fal fa-calendar"></i></span>
                                                        <input type="text" name="datepicker-here" placeholder="تاریخ کنسرت"    value=""/>
                                                        <span class="clear-singleinput"><i class="fal fa-times"></i></span>
                                                    </div>
                                                    <div class="main-search-input-item">
                                                        <select data-placeholder="Loaction" class="chosen-select on-radius" >
                                                            <option>همه شهرها</option>
                                                            <option>تهران</option>
                                                            <option>کرج</option>
                                                            <option>اصفهان</option>
                                                            <option>شیراز</option>
                                                            <option>تبریز</option>
                                                            <option>رشت</option>
                                                            <option>همدان</option>
                                                            <option>کرمان</option>
                                                        </select>
                                                    </div>
                                                    <div class="main-search-input-item">
                                                        <label><i class="fal fa-handshake-alt"></i></label>
                                                        <input type="text" placeholder="نام کنسرت یا مکان" value=""/>
                                                    </div>
                                                   
                                                   
                                              
                                                    @endif
                                                   
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--tab end-->                                
                                                           
                                                                  
                                </div>
                                <!--tabs end-->
                            </div>
                            <!-- main-search-input-tabs end-->
                            <div class="hero-categories fl-wrap">
                                <ul class="no-list-style">
                                    <li><a href="listing.html"><i class="far fa-cheeseburger"></i><span>رستوران ها</span></a></li>
                                    <li><a href="listing.html"><i class="far fa-bed"></i><span>هتل ها</span></a></li>
                                    <li><a href="listing.html"><i class="far fa-shopping-bag"></i><span>فروشگاه ها</span></a></li>
                                    <li><a href="listing.html"><i class="far fa-dumbbell"></i><span>بدنسازی</span></a></li>
                                    <li><a href="listing.html"><i class="far fa-cocktail"></i><span>کنسرت ها</span></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="header-sec-link">
                            <a href="#sec1" class="custom-scroll-link"><i class="fal fa-angle-double-down"></i></a> 
                        </div>
                    </section>
                    <!--section end-->
                    <!--section  -->
                    <section class="slw-sec" id="sec1">
                        <div class="section-title">
                            <h2>{{__('cms.best-advertisings')}}</h2>
                        
                            <span class="section-separator"></span>
                        </div>
                        <div class="listing-slider-wrap fl-wrap">
                            <div class="listing-slider fl-wrap">
                                <div class="swiper-container">
                                    <div class="swiper-wrapper">
                                        <!--  swiper-slide  -->
                                        <div class="swiper-slide">
                                            <div class="listing-slider-item fl-wrap">
                                                <!-- listing-item  -->
                                                <div class="listing-item listing_carditem">
                                                    <article class="geodir-category-listing fl-wrap">
                                                        <div class="geodir-category-img">
                                                            <div class="geodir-js-favorite_btn"><i class="fal fa-heart"></i><span>ذخیره</span></div>
                                                            <a href="listing-single.html" class="geodir-category-img-wrap fl-wrap">
                                                            <img src="{{asset('template/images/all/48.jpg')}}" alt=""> 
                                                            </a>
                                                            <div class="geodir_status_date gsd_open"><i class="fal fa-lock-open"></i>اکنون باز</div>
                                                            <div class="geodir-category-opt">
                                                                <div class="geodir-category-opt_title">
                                                                    <h4><a href="listing-single.html">رستوران گوگی</a></h4>
                                                                    <div class="geodir-category-location"><a href="#"><i class="fas fa-map-marker-alt"></i>  ایران , تهران , زعفرانیه</a></div>
                                                                </div>
                                                                <div class="listing-rating-count-wrap">
                                                                    <div class="review-score">4.1</div>
                                                                    <div class="listing-rating card-popup-rainingvis" data-starrating2="4"></div>
                                                                    <br>
                                                                    <div class="reviews-count">26 نظر</div>
                                                                </div>
                                                                <div class="listing_carditem_footer fl-wrap">
                                                                    <a class="listing-item-category-wrap" href="#">
                                                                        <div class="listing-item-category red-bg"><i class="fal fa-cheeseburger"></i></div>
                                                                        <span>رستوران</span>
                                                                    </a>
                                                                    <div class="price-level geodir-category_price">
                                                                        <span class="price-level-item" data-pricerating="2"></span>
                                                                        <span class="price-name-tooltip">مناسب</span>
                                                                    </div>
                                                                    <div class="post-author"><a href="#"><img src="{{asset('template/images/avatar/4.jpg')}}" alt=""><span>توسط ، آلیسا نوری</span></a></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </article>
                                                </div>
                                                <!-- listing-item end -->                                                   
                                            </div>
                                        </div>
                                        <!--  swiper-slide end  -->  
                                        <!--  swiper-slide  -->
                                        <div class="swiper-slide">
                                            <div class="listing-slider-item fl-wrap">
                                                <!-- listing-item  -->
                                                <div class="listing-item listing_carditem">
                                                    <article class="geodir-category-listing fl-wrap">
                                                        <div class="geodir-category-img">
                                                            <div class="geodir-js-favorite_btn"><i class="fal fa-heart"></i><span>ذخیره</span></div>
                                                            <a href="listing-single.html" class="geodir-category-img-wrap fl-wrap">
                                                            <img src="{{asset('template/images/all/25.jpg')}}" alt=""> 
                                                            </a>
                                                            <div class="geodir_status_date gsd_close"><i class="fal fa-lock"></i>بسته</div>
                                                            <div class="geodir-category-opt">
                                                                <div class="geodir-category-opt_title">
                                                                    <h4><a href="listing-single.html">سالن بدنسازی در مرکز شهر</a></h4>
                                                                    <div class="geodir-category-location"><a href="#"><i class="fas fa-map-marker-alt"></i>  ایران , تهران , زعفرانیه</a></div>
                                                                </div>
                                                                <div class="listing-rating-count-wrap">
                                                                    <div class="review-score">5.0</div>
                                                                    <div class="listing-rating card-popup-rainingvis" data-starrating2="5"></div>
                                                                    <br>
                                                                    <div class="reviews-count">7 نظر</div>
                                                                </div>
                                                                <div class="listing_carditem_footer fl-wrap">
                                                                    <a class="listing-item-category-wrap" href="#">
                                                                        <div class="listing-item-category blue-bg"><i class="fal fa-dumbbell"></i></div>
                                                                        <span>بدنسازی / باشگاه</span>
                                                                    </a>
                                                                    <div class="price-level geodir-category_price">
                                                                        <span class="price-level-item" data-pricerating="3"></span>
                                                                        <span class="price-name-tooltip">متوسط</span>
                                                                    </div>
                                                                    <div class="post-author"><a href="#"><img src="{{asset('template/images/avatar/6.jpg')}}" alt=""><span>توسط ، مارک رز</span></a></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </article>
                                                </div>
                                                <!-- listing-item end -->                                                   
                                            </div>
                                        </div>
                                        <!--  swiper-slide end  -->                                                                                                                      
                                        <!--  swiper-slide  -->
                                        <div class="swiper-slide">
                                            <div class="listing-slider-item fl-wrap">
                                                <!-- listing-item  -->
                                                <div class="listing-item listing_carditem">
                                                    <article class="geodir-category-listing fl-wrap">
                                                        <div class="geodir-category-img">
                                                            <div class="geodir-js-favorite_btn"><i class="fal fa-heart"></i><span>ذخیره</span></div>
                                                            <a href="listing-single.html" class="geodir-category-img-wrap fl-wrap">
                                                            <img src="{{asset('template/images/all/42.jpg')}}" alt=""> 
                                                            </a>
                                                            <div class="geodir_status_date gsd_open"><i class="fal fa-lock-open"></i>اکنون باز</div>
                                                            <div class="geodir-category-opt">
                                                                <div class="geodir-category-opt_title">
                                                                    <h4><a href="listing-single.html">هتل مهتاب</a></h4>
                                                                    <div class="geodir-category-location"><a href="#"><i class="fas fa-map-marker-alt"></i> ایران , تهران , زعفرانیه</a></div>
                                                                </div>
                                                                <div class="listing-rating-count-wrap">
                                                                    <div class="review-score">4.2</div>
                                                                    <div class="listing-rating card-popup-rainingvis" data-starrating2="4"></div>
                                                                    <br>
                                                                    <div class="reviews-count">3 نظر</div>
                                                                </div>
                                                                <div class="listing_carditem_footer fl-wrap">
                                                                    <a class="listing-item-category-wrap" href="#">
                                                                        <div class="listing-item-category  yellow-bg"><i class="fal fa-bed"></i></div>
                                                                        <span>هتل</span>
                                                                    </a>
                                                                    <div class="price-level geodir-category_price">
                                                                        <span class="price-level-item" data-pricerating="4"></span>
                                                                        <span class="price-name-tooltip">خیلی زیاد</span>
                                                                    </div>
                                                                    <div class="post-author"><a href="#"><img src="{{asset('template/images/avatar/5.jpg')}}" alt=""><span>توسط ، نایست وود</span></a></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </article>
                                                </div>
                                                <!-- listing-item end -->                                                   
                                            </div>
                                        </div>
                                        <!--  swiper-slide end  -->                                       
                                        <!--  swiper-slide  -->
                                        <div class="swiper-slide">
                                            <div class="listing-slider-item fl-wrap">
                                                <!-- listing-item  -->
                                                <div class="listing-item listing_carditem">
                                                    <article class="geodir-category-listing fl-wrap">
                                                        <div class="geodir-category-img">
                                                            <div class="geodir-js-favorite_btn"><i class="fal fa-heart"></i><span>ذخیره</span></div>
                                                            <a href="listing-single.html" class="geodir-category-img-wrap fl-wrap">
                                                            <img src="{{asset('template/images/all/29.jpg')}}" alt=""> 
                                                            </a>
                                                            <div class="geodir_status_date gsd_open"><i class="fal fa-lock-open"></i>اکنون باز</div>
                                                            <div class="geodir-category-opt">
                                                                <div class="geodir-category-opt_title">
                                                                    <h4><a href="listing-single.html">مرکز خرید شفق قطبی</a></h4>
                                                                    <div class="geodir-category-location"><a href="#"><i class="fas fa-map-marker-alt"></i> ایران , تهران , زعفرانیه</a></div>
                                                                </div>
                                                                <div class="listing-rating-count-wrap">
                                                                    <div class="review-score">5.0</div>
                                                                    <div class="listing-rating card-popup-rainingvis" data-starrating2="5"></div>
                                                                    <br>
                                                                    <div class="reviews-count">2 نظر</div>
                                                                </div>
                                                                <div class="listing_carditem_footer fl-wrap">
                                                                    <a class="listing-item-category-wrap" href="#">
                                                                        <div class="listing-item-category green-bg"><i class="fal fa-cart-arrow-down"></i></div>
                                                                        <span>فروشگاه</span>
                                                                    </a>
                                                                    <div class="price-level geodir-category_price">
                                                                        <span class="price-level-item" data-pricerating="4"></span>
                                                                        <span class="price-name-tooltip">خیلی زیاد</span>
                                                                    </div>
                                                                    <div class="post-author"><a href="#"><img src="{{asset('template/images/avatar/3.jpg')}}" alt=""><span>توسط ، کلیف آنتونی</span></a></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </article>
                                                </div>
                                                <!-- listing-item end -->                                                   
                                            </div>
                                        </div>
                                        <!--  swiper-slide end  -->                                        
                                    </div>
                                </div>
                                <div class="listing-carousel-button listing-carousel-button-next2"><i class="fas fa-caret-right"></i></div>
                                <div class="listing-carousel-button listing-carousel-button-prev2"><i class="fas fa-caret-left"></i></div>
                            </div>
                            <div class="tc-pagination_wrap">
                                <div class="tc-pagination2"></div>
                            </div>
                        </div>
                    </section>
                    <!--section end-->
                    <div class="sec-circle fl-wrap"></div>
                 
                    <!--section end-->
                    <section class="parallax-section small-par" data-scrollax-parent="true">
                        <div class="bg par-elem "  data-bg="{{asset('template/images/bg/22.jpg')}}" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="overlay  op7"></div>
                        <div class="container">
                            <div class=" single-facts single-facts_2 fl-wrap">
                                <!-- inline-facts -->
                                <div class="inline-facts-wrap">
                                    <div class="inline-facts">
                                        <div class="milestone-counter">
                                            <div class="stats animaper">
                                                <div class="num" data-content="0" data-num="1254">1254</div>
                                            </div>
                                        </div>
                                        <h6>بازدید کنندگان جدید هر هفته</h6>
                                    </div>
                                </div>
                                <!-- inline-facts end -->
                                <!-- inline-facts  -->
                                <div class="inline-facts-wrap">
                                    <div class="inline-facts">
                                        <div class="milestone-counter">
                                            <div class="stats animaper">
                                                <div class="num" data-content="0" data-num="12168">12168</div>
                                            </div>
                                        </div>
                                        <h6>رضایت مشتریان هر ماه</h6>
                                    </div>
                                </div>
                                <!-- inline-facts end -->
                                <!-- inline-facts  -->
                                <div class="inline-facts-wrap">
                                    <div class="inline-facts">
                                        <div class="milestone-counter">
                                            <div class="stats animaper">
                                                <div class="num" data-content="0" data-num="2172">2172</div>
                                            </div>
                                        </div>
                                        <h6>جوایز شگفت انگیز</h6>
                                    </div>
                                </div>
                                <!-- inline-facts end -->
                                <!-- inline-facts  -->
                                <div class="inline-facts-wrap">
                                    <div class="inline-facts">
                                        <div class="milestone-counter">
                                            <div class="stats animaper">
                                                <div class="num" data-content="0" data-num="732">732</div>
                                            </div>
                                        </div>
                                        <h6>تبلیغات جدید هر هفته</h6>
                                    </div>
                                </div>
                                <!-- inline-facts end -->
                            </div>
                        </div>
                    </section>
                    <!--section end--> 
                    <!--section  -->
                    <section>
                        <div class="container big-container">
                            <div class="section-title">
                                <h2><span>مکان های معروف</span></h2>
                                <div class="section-subtitle">بهترین تبلیغات</div>
                                <span class="section-separator"></span>
                                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است.</p>
                            </div>
                            <div class="listing-filters gallery-filters fl-wrap">
                                <a href="#" class="gallery-filter  gallery-filter-active" data-filter="*">{{__('cms.all')}}   </a>
                                <a href="#" class="gallery-filter" data-filter=".restaurant">رستوران ها </a>
                                <a href="#" class="gallery-filter" data-filter=".hotels">هتل ها</a>
                                <a href="#" class="gallery-filter" data-filter=".events">کنسرت ها</a>
                                <a href="#" class="gallery-filter" data-filter=".fitness">باشگاه ها</a>
                            </div>
                            <div class="grid-item-holder gallery-items fl-wrap">
                                <!--  gallery-item-->
                                <div class="gallery-item restaurant events">
                                    <!-- listing-item  -->
                                    <div class="listing-item">
                                        <article class="geodir-category-listing fl-wrap">
                                            <div class="geodir-category-img">
                                                <div class="geodir-js-favorite_btn"><i class="fal fa-heart"></i><span>ذخیره</span></div>
                                                <a href="listing-single.html" class="geodir-category-img-wrap fl-wrap">
                                                <img src="{{asset('template/images/all/1.jpg')}}" alt=""> 
                                                </a>
                                                <div class="listing-avatar"><a href="author-single.html"><img src="{{asset('template/images/avatar/1.jpg')}}" alt=""></a>
                                                    <span class="avatar-tooltip">اضافه شده توسط  <strong>آلیسا نوری</strong></span>
                                                </div>
                                                <div class="geodir_status_date gsd_open"><i class="fal fa-lock-open"></i>اکنون باز</div>
                                                <div class="geodir-category-opt">
                                                    <div class="listing-rating-count-wrap">
                                                        <div class="review-score">4.8</div>
                                                        <div class="listing-rating card-popup-rainingvis" data-starrating2="5"></div>
                                                        <br>
                                                        <div class="reviews-count">12 نظر</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="geodir-category-content fl-wrap title-sin_item">
                                                <div class="geodir-category-content-title fl-wrap">
                                                    <div class="geodir-category-content-title-item">
                                                        <h3 class="title-sin_map"><a href="listing-single.html">رستوران مجلل</a><span class="verified-badge"><i class="fal fa-check"></i></span></h3>
                                                        <div class="geodir-category-location fl-wrap"><a href="#" ><i class="fas fa-map-marker-alt"></i> ایران , تهران , زعفرانیه</a></div>
                                                    </div>
                                                </div>
                                                <div class="geodir-category-text fl-wrap">
                                                    <p class="small-text">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است.</p>
                                                    <div class="facilities-list fl-wrap">
                                                        <div class="facilities-list-title">امکانات : </div>
                                                        <ul class="no-list-style">
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="وای فای رایگان"><i class="fal fa-wifi"></i></li>
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="پارکینگ"><i class="fal fa-parking"></i></li>
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="سیگار ممنوع"><i class="fal fa-smoking-ban"></i></li>
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="حیوانات خانگی"><i class="fal fa-dog-leashed"></i></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="geodir-category-footer fl-wrap">
                                                    <a class="listing-item-category-wrap">
                                                        <div class="listing-item-category red-bg"><i class="fal fa-cheeseburger"></i></div>
                                                        <span>رستوران ها</span>
                                                    </a>
                                                    <div class="geodir-opt-list">
                                                        <ul class="no-list-style">
                                                            <li><a href="#" class="show_gcc"><i class="fal fa-envelope"></i><span class="geodir-opt-tooltip">اطلاعات تماس</span></a></li>
                                                            <li><a href="#1" class="single-map-item" data-newlatitude="35.725705" data-newlongitude="51.408263"><i class="fal fa-map-marker-alt"></i><span class="geodir-opt-tooltip">روی نقشه <strong>1</strong></span> </a></li>
                                                            <li>
                                                                <div class="dynamic-gal gdop-list-link" data-dynamicPath="[{'src': 'images/all/1.jpg'},{'src': 'images/all/24.jpg'}, {'src': 'images/all/12.jpg'}]"><i class="fal fa-search-plus"></i><span class="geodir-opt-tooltip"> گالری </span></div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="price-level geodir-category_price">
                                                        <span class="price-level-item" data-pricerating="3"></span>
                                                        <span class="price-name-tooltip">قیمت</span>
                                                    </div>
                                                    <div class="geodir-category_contacts">
                                                        <div class="close_gcc"><i class="fal fa-times-circle"></i></div>
                                                        <ul class="no-list-style">
                                                            <li><span><i class="fal fa-phone"></i> تماس : </span><a href="#">021-1234567</a></li>
                                                            <li><span><i class="fal fa-envelope"></i> ایمیل : </span><a href="#">yourmail@domain.com</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                    <!-- listing-item end -->                              
                                </div>
                                <!-- gallery-item  end-->
                                <!--  gallery-item-->
                                <div class="gallery-item events">
                                    <!-- listing-item  -->
                                    <div class="listing-item">
                                        <article class="geodir-category-listing fl-wrap">
                                            <div class="geodir-category-img">
                                                <div class="geodir-js-favorite_btn"><i class="fal fa-heart"></i><span>ذخیره</span></div>
                                                <a href="listing-single.html" class="geodir-category-img-wrap fl-wrap">
                                                <img src="{{asset('template/images/all/9.jpg')}}" alt=""> 
                                                </a>
                                                <div class="listing-avatar"><a href="author-single.html"><img src="{{asset('template/images/avatar/2.jpg')}}" alt=""></a>
                                                    <span class="avatar-tooltip">اضافه شده توسط  <strong>محمد سعیدی</strong></span>
                                                </div>
                                                <div class="geodir_status_date color-bg"><i class="fal fa-clock"></i>27 اسفند 1399</div>
                                                <div class="geodir-category-opt">
                                                    <div class="listing-rating-count-wrap">
                                                        <div class="review-score">4.2</div>
                                                        <div class="listing-rating card-popup-rainingvis" data-starrating2="4"></div>
                                                        <br>                                                
                                                        <div class="reviews-count">6 نظر</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="geodir-category-content fl-wrap title-sin_item">
                                                <div class="geodir-category-content-title fl-wrap ">
                                                    <div class="geodir-category-content-title-item">
                                                        <h3 class="title-sin_map"><a href="listing-single.html">کنسرت</a></h3>
                                                        <div class="geodir-category-location fl-wrap"><a href="#"><i class="fas fa-map-marker-alt"></i>  ایران , تهران , زعفرانیه</a></div>
                                                    </div>
                                                </div>
                                                <div class="geodir-category-text fl-wrap">
                                                    <p class="small-text">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است.</p>
                                                    <div class="facilities-list fl-wrap">
                                                        <div class="facilities-list-title">امکانات : </div>
                                                        <ul class="no-list-style">
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="وای فای رایگان"><i class="fal fa-wifi"></i></li>
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="پارکینگ"><i class="fal fa-parking"></i></li>
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="سیگار ممنوع"><i class="fal fa-smoking-ban"></i></li>
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="حیوانات خانگی"><i class="fal fa-dog-leashed"></i></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="geodir-category-footer fl-wrap">
                                                    <a class="listing-item-category-wrap">
                                                        <div class="listing-item-category purp-bg"><i class="fal fa-cocktail"></i></div>
                                                        <span>کنسرت</span>
                                                    </a>
                                                    <div class="geodir-opt-list">
                                                        <ul class="no-list-style">
                                                            <li><a href="#" class="show_gcc"><i class="fal fa-envelope"></i><span class="geodir-opt-tooltip">اطلاعات تماس</span></a></li>
                                                            <li><a href="#" class="single-map-item" data-newlatitude="35.725705" data-newlongitude="51.408263"><i class="fal fa-map-marker-alt"></i><span class="geodir-opt-tooltip">روی نقشه <strong>2</strong></span></a></li>
                                                            <li>
                                                                <div class="dynamic-gal gdop-list-link" data-dynamicPath="[{'src': 'images/all/9.jpg'},{'src': 'images/all/32.jpg'}, {'src': 'images/all/23.jpg'}]"><i class="fal fa-search-plus"></i><span class="geodir-opt-tooltip"> گالری </span></div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="price-level geodir-category_price">
                                                        <span class="price-level-item" data-pricerating="4"></span>
                                                        <span class="price-name-tooltip">قیمت</span>
                                                    </div>
                                                    <div class="geodir-category_contacts">
                                                        <div class="close_gcc"><i class="fal fa-times-circle"></i></div>
                                                        <ul class="no-list-style">
                                                            <li><span><i class="fal fa-phone"></i> تماس : </span><a href="#">021-1234567</a></li>
                                                            <li><span><i class="fal fa-envelope"></i> ایمیل : </span><a href="#">yourmail@domain.com</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                    <!-- listing-item end -->  
                                </div>
                                <!-- gallery-item  end-->         
                                <!-- gallery-item  -->
                                <div class="gallery-item fitness">
                                    <!-- listing-item  -->
                                    <div class="listing-item">
                                        <article class="geodir-category-listing fl-wrap">
                                            <div class="geodir-category-img">
                                                <div class="geodir-js-favorite_btn"><i class="fal fa-heart"></i><span>ذخیره</span></div>
                                                <a href="listing-single.html" class="geodir-category-img-wrap fl-wrap">
                                                <img src="{{asset('template/images/all/31.jpg')}}" alt=""> 
                                                </a>
                                                <div class="listing-avatar"><a href="author-single.html"><img src="{{asset('template/images/avatar/4.jpg')}}" alt=""></a>
                                                    <span class="avatar-tooltip">اضافه شده توسط  <strong>هستی جعفری</strong></span>
                                                </div>
                                                <div class="geodir_status_date gsd_close"><i class="fal fa-lock"></i>بسته</div>
                                                <div class="geodir-category-opt">
                                                    <div class="listing-rating-count-wrap">
                                                        <div class="review-score">3.8</div>
                                                        <div class="listing-rating card-popup-rainingvis" data-starrating2="3"></div>
                                                        <br>
                                                        <div class="reviews-count">4 نظر</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="geodir-category-content fl-wrap title-sin_item">
                                                <div class="geodir-category-content-title fl-wrap">
                                                    <div class="geodir-category-content-title-item">
                                                        <h3 class="title-sin_map"><a href="listing-single.html">باشگاه بدنسازی</a><span class="verified-badge"><i class="fal fa-check"></i></span></h3>
                                                        <div class="geodir-category-location fl-wrap"><a href="#"><i class="fas fa-map-marker-alt"></i> ایران , تهران , زعفرانیه</a></div>
                                                    </div>
                                                </div>
                                                <div class="geodir-category-text fl-wrap">
                                                    <p class="small-text">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است.</p>
                                                    <div class="facilities-list fl-wrap">
                                                        <div class="facilities-list-title">امکانات : </div>
                                                        <ul class="no-list-style">
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="وای فای رایگان"><i class="fal fa-wifi"></i></li>
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="پارکینگ"><i class="fal fa-parking"></i></li>
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="سیگار ممنوع"><i class="fal fa-smoking-ban"></i></li>
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="حیوانات خانگی"><i class="fal fa-dog-leashed"></i></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="geodir-category-footer fl-wrap">
                                                    <a class="listing-item-category-wrap">
                                                        <div class="listing-item-category blue-bg"><i class="fal fa-dumbbell"></i></div>
                                                        <span>باشگاه</span>
                                                    </a>
                                                    <div class="geodir-opt-list">
                                                        <ul class="no-list-style">
                                                            <li><a href="#" class="show_gcc"><i class="fal fa-envelope"></i><span class="geodir-opt-tooltip">اطلاعات تماس</span></a></li>
                                                            <li><a href="#" class="single-map-item" data-newlatitude="35.725705" data-newlongitude="51.408263"><i class="fal fa-map-marker-alt"></i><span class="geodir-opt-tooltip">روی نقشه <strong>3</strong></span> </a></li>
                                                            <li>
                                                                <div class="dynamic-gal gdop-list-link" data-dynamicPath="[{'src': 'images/all/31.jpg'},{'src': 'images/all/10.jpg'}, {'src': 'images/all/15.jpg'}]"><i class="fal fa-search-plus"></i><span class="geodir-opt-tooltip"> گالری </span></div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="price-level geodir-category_price">
                                                        <span class="price-level-item" data-pricerating="2"></span>
                                                        <span class="price-name-tooltip">قیمت</span>
                                                    </div>
                                                    <div class="geodir-category_contacts">
                                                        <div class="close_gcc"><i class="fal fa-times-circle"></i></div>
                                                        <ul class="no-list-style">
                                                            <li><span><i class="fal fa-phone"></i> تماس : </span><a href="#">021-1234567</a></li>
                                                            <li><span><i class="fal fa-envelope"></i> ایمیل : </span><a href="#">yourmail@domain.com</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                    <!-- listing-item end -->                                   
                                </div>
                                <!-- gallery-item  end-->  
                                <!-- gallery-item  -->
                                <div class="gallery-item hotels">
                                    <!-- listing-item  -->
                                    <div class="listing-item">
                                        <article class="geodir-category-listing fl-wrap">
                                            <div class="geodir-category-img">
                                                <div class="geodir-js-favorite_btn"><i class="fal fa-heart"></i><span>ذخیره</span></div>
                                                <a href="listing-single.html" class="geodir-category-img-wrap fl-wrap">
                                                <img src="{{asset('template/images/all/16.jpg')}}" alt=""> 
                                                </a>
                                                <div class="listing-avatar"><a href="author-single.html"><img src="{{asset('template/images/avatar/3.jpg')}}" alt=""></a>
                                                    <span class="avatar-tooltip">اضافه شده توسط  <strong>بهمن یوسفی</strong></span>
                                                </div>
                                                <div class="geodir_status_date gsd_open"><i class="fal fa-lock-open"></i>اکنون باز</div>
                                                <div class="geodir-category-opt">
                                                    <div class="listing-rating-count-wrap">
                                                        <div class="review-score">5.0</div>
                                                        <div class="listing-rating card-popup-rainingvis" data-starrating2="5"></div>
                                                        <br>                                                
                                                        <div class="reviews-count">4 نظر</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="geodir-category-content fl-wrap title-sin_item">
                                                <div class="geodir-category-content-title fl-wrap">
                                                    <div class="geodir-category-content-title-item">
                                                        <h3 class="title-sin_map"><a href="listing-single.html">هتل مونت پلاسا</a><span class="verified-badge"><i class="fal fa-check"></i></span></h3>
                                                        <div class="geodir-category-location fl-wrap"><a href="#"><i class="fas fa-map-marker-alt"></i>  ایران , تهران , زعفرانیه</a></div>
                                                    </div>
                                                </div>
                                                <div class="geodir-category-text fl-wrap">
                                                    <p class="small-text">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است.</p>
                                                    <div class="facilities-list fl-wrap">
                                                        <div class="facilities-list-title">امکانات : </div>
                                                        <ul class="no-list-style">
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="وای فای رایگان"><i class="fal fa-wifi"></i></li>
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="پارکینگ"><i class="fal fa-parking"></i></li>
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="سیگار ممنوع"><i class="fal fa-smoking-ban"></i></li>
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="حیوانات خانگی"><i class="fal fa-dog-leashed"></i></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="geodir-category-footer fl-wrap">
                                                    <a class="listing-item-category-wrap">
                                                        <div class="listing-item-category  yellow-bg"><i class="fal fa-bed"></i></div>
                                                        <span>هتل</span>
                                                    </a>
                                                    <div class="geodir-opt-list">
                                                        <ul class="no-list-style">
                                                            <li><a href="#" class="show_gcc"><i class="fal fa-envelope"></i><span class="geodir-opt-tooltip">اطلاعات تماس</span></a></li>
                                                            <li><a href="#" class="single-map-item" data-newlatitude="35.725705" data-newlongitude="51.408263"><i class="fal fa-map-marker-alt"></i><span class="geodir-opt-tooltip">روی نقشه <strong>4</strong></span></a></li>
                                                            <li>
                                                                <div class="dynamic-gal gdop-list-link" data-dynamicPath="[{'src': 'images/all/16.jpg'},{'src': 'images/all/27.jpg'}, {'src': 'images/all/20.jpg'}]"><i class="fal fa-search-plus"></i><span class="geodir-opt-tooltip"> گالری </span></div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="price-level geodir-category_price">
                                                        <span class="price-level-item" data-pricerating="4"></span>
                                                        <span class="price-name-tooltip">قیمت</span>
                                                    </div>
                                                    <div class="geodir-category_contacts">
                                                        <div class="close_gcc"><i class="fal fa-times-circle"></i></div>
                                                        <ul class="no-list-style">
                                                            <li><span><i class="fal fa-phone"></i> تماس : </span><a href="#">021-1234567</a></li>
                                                            <li><span><i class="fal fa-envelope"></i> ایمیل : </span><a href="#">yourmail@domain.com</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                    <!-- listing-item end -->                                                            
                                </div>
                                <!-- gallery-item  end-->                                  
                                <!-- gallery-item  -->
                                <div class="gallery-item hotels">
                                    <!-- listing-item  -->
                                    <div class="listing-item">
                                        <article class="geodir-category-listing fl-wrap">
                                            <div class="geodir-category-img">
                                                <div class="geodir-js-favorite_btn"><i class="fal fa-heart"></i><span>ذخیره</span></div>
                                                <a href="listing-single.html" class="geodir-category-img-wrap fl-wrap">
                                                <img src="{{asset('template/images/all/28.jpg')}}" alt=""> 
                                                </a>
                                                <div class="listing-avatar"><a href="author-single.html"><img src="{{asset('template/images/avatar/5.jpg')}}" alt=""></a>
                                                    <span class="avatar-tooltip">اضافه شده توسط  <strong>محبوبه سیاری</strong></span>
                                                </div>
                                                <div class="geodir_status_date gsd_open"><i class="fal fa-lock-open"></i>اکنون باز</div>
                                                <div class="geodir-category-opt">
                                                    <div class="listing-rating-count-wrap">
                                                        <div class="review-score">4.7</div>
                                                        <div class="listing-rating card-popup-rainingvis" data-starrating2="5"></div>
                                                        <br>
                                                        <div class="reviews-count">9 نظر</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="geodir-category-content fl-wrap title-sin_item">
                                                <div class="geodir-category-content-title fl-wrap">
                                                    <div class="geodir-category-content-title-item">
                                                        <h3 class="title-sin_map"><a href="listing-single.html">فروشگاه صنایع  دستی</a></h3>
                                                        <div class="geodir-category-location fl-wrap"><a href="#"><i class="fas fa-map-marker-alt"></i>  ایران , تهران , زعفرانیه</a></div>
                                                    </div>
                                                </div>
                                                <div class="geodir-category-text fl-wrap">
                                                    <p class="small-text">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است.</p>
                                                    <div class="facilities-list fl-wrap">
                                                        <div class="facilities-list-title">امکانات : </div>
                                                        <ul class="no-list-style">
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="وای فای رایگان"><i class="fal fa-wifi"></i></li>
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="پارکینگ"><i class="fal fa-parking"></i></li>
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="سیگار ممنوع"><i class="fal fa-smoking-ban"></i></li>
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="حیوانات خانگی"><i class="fal fa-dog-leashed"></i></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="geodir-category-footer fl-wrap">
                                                    <a class="listing-item-category-wrap">
                                                        <div class="listing-item-category green-bg"><i class="fal fa-cart-arrow-down"></i></div>
                                                        <span>فروشگاه</span>
                                                    </a>
                                                    <div class="geodir-opt-list">
                                                        <ul class="no-list-style">
                                                            <li><a href="#" class="show_gcc"><i class="fal fa-envelope"></i><span class="geodir-opt-tooltip">اطلاعات تماس</span></a></li>
                                                            <li><a href="#" class="single-map-item" data-newlatitude="35.725705" data-newlongitude="51.408263"><i class="fal fa-map-marker-alt"></i><span class="geodir-opt-tooltip">روی نقشه <strong>5</strong></span> </a></li>
                                                            <li>
                                                                <div class="dynamic-gal gdop-list-link" data-dynamicPath="[{'src': 'images/all/28.jpg'},{'src': 'images/all/29.jpg'}, {'src': 'images/all/30.jpg'}]"><i class="fal fa-search-plus"></i><span class="geodir-opt-tooltip"> گالری </span></div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="price-level geodir-category_price">
                                                        <span class="price-level-item" data-pricerating="3"></span>
                                                        <span class="price-name-tooltip">قیمت</span>
                                                    </div>
                                                    <div class="geodir-category_contacts">
                                                        <div class="close_gcc"><i class="fal fa-times-circle"></i></div>
                                                        <ul class="no-list-style">
                                                            <li><span><i class="fal fa-phone"></i> تماس : </span><a href="#">021-1234567</a></li>
                                                            <li><span><i class="fal fa-envelope"></i> ایمیل : </span><a href="#">yourmail@domain.com</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                    <!-- listing-item end -->                                           
                                </div>
                                <!-- gallery-item  end-->                                 
                                <!-- gallery-item  -->
                                <div class="gallery-item  restaurant hotels">
                                    <!-- listing-item  -->
                                    <div class="listing-item">
                                        <article class="geodir-category-listing fl-wrap">
                                            <div class="geodir-category-img">
                                                <div class="geodir-js-favorite_btn"><i class="fal fa-heart"></i><span>ذخیره</span></div>
                                                <a href="listing-single.html" class="geodir-category-img-wrap fl-wrap">
                                                <img src="{{asset('template/images/all/18.jpg')}}" alt=""> 
                                                </a>
                                                <div class="listing-avatar"><a href="author-single.html"><img src="{{asset('template/images/avatar/1.jpg')}}" alt=""></a>
                                                    <span class="avatar-tooltip">اضافه شده توسط  <strong>آلیسا نوری</strong></span>
                                                </div>
                                                <div class="geodir_status_date gsd_close"><i class="fal fa-lock-open"></i>بسته</div>
                                                <div class="geodir-category-opt">
                                                    <div class="listing-rating-count-wrap">
                                                        <div class="review-score">4.1</div>
                                                        <div class="listing-rating card-popup-rainingvis" data-starrating2="4"></div>
                                                        <br>
                                                        <div class="reviews-count">26 نظر</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="geodir-category-content fl-wrap title-sin_item">
                                                <div class="geodir-category-content-title fl-wrap">
                                                    <div class="geodir-category-content-title-item">
                                                        <h3 class="title-sin_map"><a href="listing-single.html">کافه دوستی در جردن</a><span class="verified-badge"><i class="fal fa-check"></i></span></h3>
                                                        <div class="geodir-category-location fl-wrap"><a href="#"><i class="fas fa-map-marker-alt"></i> ایران , تهران , زعفرانیه</a></div>
                                                    </div>
                                                </div>
                                                <div class="geodir-category-text fl-wrap">
                                                    <p class="small-text">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است.</p>
                                                    <div class="facilities-list fl-wrap">
                                                        <div class="facilities-list-title">امکانات : </div>
                                                        <ul class="no-list-style">
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="وای فای رایگان"><i class="fal fa-wifi"></i></li>
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="پارکینگ"><i class="fal fa-parking"></i></li>
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="سیگار ممنوع"><i class="fal fa-smoking-ban"></i></li>
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="حیوانات خانگی"><i class="fal fa-dog-leashed"></i></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="geodir-category-footer fl-wrap">
                                                    <a class="listing-item-category-wrap">
                                                        <div class="listing-item-category red-bg"><i class="fal fa-cheeseburger"></i></div>
                                                        <span>رستوران</span>
                                                    </a>
                                                    <div class="geodir-opt-list">
                                                        <ul class="no-list-style">
                                                            <li><a href="#" class="show_gcc"><i class="fal fa-envelope"></i><span class="geodir-opt-tooltip">اطلاعات تماس</span></a></li>
                                                            <li><a href="#" class="single-map-item" data-newlatitude="35.725705" data-newlongitude="51.408263"><i class="fal fa-map-marker-alt"></i><span class="geodir-opt-tooltip">روی نقشه <strong>6</strong></span> </a></li>
                                                            <li>
                                                                <div class="dynamic-gal gdop-list-link" data-dynamicPath="[{'src': 'images/all/18.jpg'},{'src': 'images/all/21.jpg'}, {'src': 'images/all/22.jpg'}]"><i class="fal fa-search-plus"></i><span class="geodir-opt-tooltip"> گالری </span></div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="price-level geodir-category_price">
                                                        <span class="price-level-item" data-pricerating="2"></span>
                                                        <span class="price-name-tooltip">قیمت</span>
                                                    </div>
                                                    <div class="geodir-category_contacts">
                                                        <div class="close_gcc"><i class="fal fa-times-circle"></i></div>
                                                        <ul class="no-list-style">
                                                            <li><span><i class="fal fa-phone"></i> تماس : </span><a href="#">021-1234567</a></li>
                                                            <li><span><i class="fal fa-envelope"></i> ایمیل : </span><a href="#">yourmail@domain.com</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                    <!-- listing-item end -->                                                        
                                </div>
                                <!-- gallery-item  end-->
                                <!-- gallery-item  -->
                                <div class="gallery-item fitness">
                                    <!-- listing-item  -->
                                    <div class="listing-item">
                                        <article class="geodir-category-listing fl-wrap">
                                            <div class="geodir-category-img">
                                                <div class="geodir-js-favorite_btn"><i class="fal fa-heart"></i><span>ذخیره</span></div>
                                                <a href="listing-single.html" class="geodir-category-img-wrap fl-wrap">
                                                <img src="{{asset('template/images/all/52.jpg')}}" alt=""> 
                                                </a>
                                                <div class="listing-avatar"><a href="author-single.html"><img src="{{asset('template/images/avatar/6.jpg')}}" alt=""></a>
                                                    <span class="avatar-tooltip">اضافه شده توسط  <strong>محمود احمدی</strong></span>
                                                </div>
                                                <div class="geodir_status_date gsd_close"><i class="fal fa-lock"></i>بسته</div>
                                                <div class="geodir-category-opt">
                                                    <div class="listing-rating-count-wrap">
                                                        <div class="review-score">4.1</div>
                                                        <div class="listing-rating card-popup-rainingvis" data-starrating2="4"></div>
                                                        <br>
                                                        <div class="reviews-count">56 نظر</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="geodir-category-content fl-wrap title-sin_item">
                                                <div class="geodir-category-content-title fl-wrap">
                                                    <div class="geodir-category-content-title-item">
                                                        <h3 class="title-sin_map"><a href="listing-single.html">بدنسازی قدرتی</a><span class="verified-badge"><i class="fal fa-check"></i></span></h3>
                                                        <div class="geodir-category-location fl-wrap"><a href="#"><i class="fas fa-map-marker-alt"></i> ایران , تهران , زعفرانیه</a></div>
                                                    </div>
                                                </div>
                                                <div class="geodir-category-text fl-wrap">
                                                    <p class="small-text">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است.</p>
                                                    <div class="facilities-list fl-wrap">
                                                        <div class="facilities-list-title">امکانات : </div>
                                                        <ul class="no-list-style">
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="وای فای رایگان"><i class="fal fa-wifi"></i></li>
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="پارکینگ"><i class="fal fa-parking"></i></li>
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="سیگار ممنوع"><i class="fal fa-smoking-ban"></i></li>
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="حیوانات خانگی"><i class="fal fa-dog-leashed"></i></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="geodir-category-footer fl-wrap">
                                                    <a class="listing-item-category-wrap">
                                                        <div class="listing-item-category blue-bg"><i class="fal fa-dumbbell"></i></div>
                                                        <span>باشگاه</span>
                                                    </a>
                                                    <div class="geodir-opt-list">
                                                        <ul class="no-list-style">
                                                            <li><a href="#" class="show_gcc"><i class="fal fa-envelope"></i><span class="geodir-opt-tooltip">اطلاعات تماس</span></a></li>
                                                            <li><a href="#" class="single-map-item" data-newlatitude="35.725705" data-newlongitude="51.408263"><i class="fal fa-map-marker-alt"></i><span class="geodir-opt-tooltip">روی نقشه <strong>7</strong></span> </a></li>
                                                            <li>
                                                                <div class="dynamic-gal gdop-list-link" data-dynamicPath="[{'src': 'images/all/10.jpg'},{'src': 'images/all/14.jpg'}]"><i class="fal fa-search-plus"></i><span class="geodir-opt-tooltip"> گالری </span></div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="price-level geodir-category_price">
                                                        <span class="price-level-item" data-pricerating="2"></span>
                                                        <span class="price-name-tooltip">قیمت</span>
                                                    </div>
                                                    <div class="geodir-category_contacts">
                                                        <div class="close_gcc"><i class="fal fa-times-circle"></i></div>
                                                        <ul class="no-list-style">
                                                            <li><span><i class="fal fa-phone"></i> تماس : </span><a href="#">021-1234567</a></li>
                                                            <li><span><i class="fal fa-envelope"></i> ایمیل : </span><a href="#">yourmail@domain.com</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                    <!-- listing-item end -->                                   
                                </div>
                                <!-- gallery-item  end--> 
                                <!-- gallery-item  -->
                                <div class="gallery-item hotels">
                                    <!-- listing-item  -->
                                    <div class="listing-item">
                                        <article class="geodir-category-listing fl-wrap">
                                            <div class="geodir-category-img">
                                                <div class="geodir-js-favorite_btn"><i class="fal fa-heart"></i><span>ذخیره</span></div>
                                                <a href="listing-single.html" class="geodir-category-img-wrap fl-wrap">
                                                <img src="{{asset('template/images/all/62.jpg')}}" alt=""> 
                                                </a>
                                                <div class="listing-avatar"><a href="author-single.html"><img src="{{asset('template/images/avatar/4.jpg')}}" alt=""></a>
                                                    <span class="avatar-tooltip">اضافه شده توسط  <strong>نسترن همتی</strong></span>
                                                </div>
                                                <div class="geodir_status_date gsd_open"><i class="fal fa-lock-open"></i>اکنون باز</div>
                                                <div class="geodir-category-opt">
                                                    <div class="listing-rating-count-wrap">
                                                        <div class="review-score">4.7</div>
                                                        <div class="listing-rating card-popup-rainingvis" data-starrating2="5"></div>
                                                        <br>
                                                        <div class="reviews-count">9 نظر</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="geodir-category-content fl-wrap title-sin_item">
                                                <div class="geodir-category-content-title fl-wrap">
                                                    <div class="geodir-category-content-title-item">
                                                        <h3 class="title-sin_map"><a href="listing-single.html">فروشگاه خانوادگی</a></h3>
                                                        <div class="geodir-category-location fl-wrap"><a href="#"><i class="fas fa-map-marker-alt"></i>  ایران , تهران , زعفرانیه</a></div>
                                                    </div>
                                                </div>
                                                <div class="geodir-category-text fl-wrap">
                                                    <p class="small-text">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است.</p>
                                                    <div class="facilities-list fl-wrap">
                                                        <div class="facilities-list-title">امکانات : </div>
                                                        <ul class="no-list-style">
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="وای فای رایگان"><i class="fal fa-wifi"></i></li>
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="پارکینگ"><i class="fal fa-parking"></i></li>
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="سیگار ممنوع"><i class="fal fa-smoking-ban"></i></li>
                                                            <li class="tolt"  data-microtip-position="top" data-tooltip="حیوانات خانگی"><i class="fal fa-dog-leashed"></i></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="geodir-category-footer fl-wrap">
                                                    <a class="listing-item-category-wrap">
                                                        <div class="listing-item-category green-bg"><i class="fal fa-cart-arrow-down"></i></div>
                                                        <span>فروشگاه</span>
                                                    </a>
                                                    <div class="geodir-opt-list">
                                                        <ul class="no-list-style">
                                                            <li><a href="#" class="show_gcc"><i class="fal fa-envelope"></i><span class="geodir-opt-tooltip">اطلاعات تماس</span></a></li>
                                                            <li><a href="#" class="single-map-item" data-newlatitude="35.725705" data-newlongitude="51.408263"><i class="fal fa-map-marker-alt"></i><span class="geodir-opt-tooltip">روی نقشه <strong>5</strong></span> </a></li>
                                                            <li>
                                                                <div class="dynamic-gal gdop-list-link" data-dynamicPath="[{'src': 'images/all/28.jpg'},{'src': 'images/all/29.jpg'}, {'src': 'images/all/30.jpg'}]"><i class="fal fa-search-plus"></i><span class="geodir-opt-tooltip"> گالری </span></div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="price-level geodir-category_price">
                                                        <span class="price-level-item" data-pricerating="3"></span>
                                                        <span class="price-name-tooltip">قیمت</span>
                                                    </div>
                                                    <div class="geodir-category_contacts">
                                                        <div class="close_gcc"><i class="fal fa-times-circle"></i></div>
                                                        <ul class="no-list-style">
                                                            <li><span><i class="fal fa-phone"></i> تماس : </span><a href="#">021-1234567</a></li>
                                                            <li><span><i class="fal fa-envelope"></i> ایمیل : </span><a href="#">yourmail@domain.com</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                    <!-- listing-item end -->                                           
                                </div>
                                <!-- gallery-item  end-->                                                                                                                        
                            </div>
                            <a href="listing.html" class="btn  dec_btn  color2-bg">تمام تبلیغات<i class="fal fa-arrow-alt-left"></i></a>
                        </div>
                    </section>
                    <!--section end-->                                        					
                    <!--section  -->
                    <section class="parallax-section" data-scrollax-parent="true">
                        <div class="bg par-elem "  data-bg="{{asset('template/images/bg/11.jpg')}}" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="overlay op7"></div>
                        <!--container-->
                        <div class="container">
                            <div class="video_section-title fl-wrap">
                                <h4>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم</h4>
                                <h2>برای شروع سفر هیجان انگیز خود آماده شوید. <br> سایت ما شما را در دنیای شگفت انگیز دیجیتال هدایت می کند</h2>
                            </div>
                            <a href="https://vimeo.com/70851162" class="promo-link big_prom   image-popup"><i class="fal fa-play"></i><span>فیلم تبلیغاتی</span></a>
                        </div>
                    </section>
                    <!--section end-->
                    <!--section  -->
                    <section      data-scrollax-parent="true">
                        <div class="container">
                            <div class="section-title">
                                <h2>ما چگونه کار می کنیم</h2>
                                <div class="section-subtitle">جستجو  و تماس </div>
                                <span class="section-separator"></span>
                                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه.</p>
                            </div>
                            <div class="process-wrap fl-wrap">
                                <ul class="no-list-style">
                                    <li>
                                        <div class="process-item">
                                            <span class="process-count">01 </span>
                                            <div class="time-line-icon"><i class="fal fa-map-marker-alt"></i></div>
                                            <h4> مکان جالب را پیدا کنید</h4>
                                            <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه.</p>
                                        </div>
                                        <span class="pr-dec"></span>
                                    </li>
                                    <li>
                                        <div class="process-item">
                                            <span class="process-count">02</span>
                                            <div class="time-line-icon"><i class="fal fa-mail-bulk"></i></div>
                                            <h4> با ما تماس بگیرید</h4>
                                            <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه.</p>
                                        </div>
                                        <span class="pr-dec"></span>
                                    </li>
                                    <li>
                                        <div class="process-item">
                                            <span class="process-count">03</span>
                                            <div class="time-line-icon"><i class="fal fa-layer-plus"></i></div>
                                            <h4> ایجاد یک تبلیغ</h4>
                                            <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه.</p>
                                        </div>
                                    </li>
                                </ul>
                                <div class="process-end"><i class="fal fa-check"></i></div>
                            </div>
                        </div>
                    </section>
                    <!--section end-->
                    
                            
                    <!--section  -->
                    <section class="gray-bg">
                        <div class="container">
                            <div class="clients-carousel-wrap fl-wrap">
                                <div class="cc-btn   cc-prev"><i class="fal fa-angle-left"></i></div>
                                <div class="cc-btn cc-next"><i class="fal fa-angle-right"></i></div>
                                <div class="clients-carousel">
                                    <div class="swiper-container">
                                        <div class="swiper-wrapper">
                                            <!--client-item-->
                                            <div class="swiper-slide">
                                                <a href="#" class="client-item"><img src="{{asset('template/images/clients/1.png')}}" alt=""></a>
                                            </div>
                                            <!--client-item end-->
                                            <!--client-item-->
                                            <div class="swiper-slide">
                                                <a href="#" class="client-item"><img src="{{asset('template/images/clients/2.png')}}" alt=""></a>
                                            </div>
                                            <!--client-item end-->
                                            <!--client-item-->
                                            <div class="swiper-slide">
                                                <a href="#" class="client-item"><img src="{{asset('template/images/clients/3.png')}}" alt=""></a>
                                            </div>
                                            <!--client-item end-->
                                            <!--client-item-->
                                            <div class="swiper-slide">
                                                <a href="#" class="client-item"><img src="{{asset('template/images/clients/1.png')}}" alt=""></a>
                                            </div>
                                            <!--client-item end-->
                                            <!--client-item-->
                                            <div class="swiper-slide">
                                                <a href="#" class="client-item"><img src="{{asset('template/images/clients/2.png')}}" alt=""></a>
                                            </div>
                                            <!--client-item end-->
                                            <!--client-item-->
                                            <div class="swiper-slide">
                                                <a href="#" class="client-item"><img src="{{asset('template/images/clients/3.png')}}" alt=""></a>
                                            </div>
                                            <!--client-item end-->                                                                                                                                                                                                                                                
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--section end-->
                </div>
                <!--content end-->



@endsection





