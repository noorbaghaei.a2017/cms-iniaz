@extends('template.app')

@section('content')


    <!-- Page Breadcrumb -->
    <section class="page-breadcrumb">
        <div class="image-layer" style="background-image:url({{asset('template/images/background/1.png')}})"></div>
        <div class="container">
            <div class="clearfix">
                <div class="pull-right">
                </div>
                <div class="pull-left">

                </div>
            </div>
        </div>
    </section>
    <!-- End Page Breadcrumb -->

    <!-- Services Page Section -->
    <section class="services-page-section">
        <div class="container">
            <div class="row box-services">
            @foreach($items as $item)
                <!-- Services Block Three -->
                    <div class="services-block-three col-lg-4 col-md-6 col-sm-12">
                        <div class="inner-box">
                            <div class="image">
                                <a href="{{route('articles.single',['article'=>convert_lang($item,LaravelLocalization::getCurrentLocale(),'slug')])}}">
                                    @if(!$item->Hasmedia('images'))
                                        <img class="img-fluid" src="{{asset('img/no-img.gif')}}" alt="{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}" title="{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}" >
                                    @else
                                        <img class="img-fluid" src="{{$item->getFirstMediaUrl('images')}}" alt="{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}" title="{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}" >
                                    @endif

                                </a>
                            </div>
                            <div class="lower-content">
                                <div class="icon-box">
                                    <span class="icon icon-heart1" style="color: #dbb16b"></span>
                                </div>
                                <h3><a href="{{route('articles.single',['article'=>convert_lang($item,LaravelLocalization::getCurrentLocale(),'slug')])}}">{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}</a></h3>
                                <p class="text">
                                    {!! convert_lang($item,LaravelLocalization::getCurrentLocale(),'text') !!}
                                </p>
                            </div>
                        </div>
                    </div>

                @endforeach



            </div>
        </div>
    </section>
    <!-- End Services Page Section -->



@endsection
