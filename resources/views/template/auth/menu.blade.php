  <!--  dashboard-menu-->
  <div class="col-md-3">
                                <div class="mob-nav-content-btn color2-bg init-dsmen fl-wrap"><i class="fal fa-bars"></i> منوی داشبورد</div>
                                <div class="clearfix"></div>
                                <div class="fixed-bar fl-wrap" id="dash_menu">
                                    <div class="user-profile-menu-wrap fl-wrap block_box">
                                        <!-- user-profile-menu-->
                                        <div class="user-profile-menu">
                                            <h3>{{__('cms.main')}}</h3>
                                            <ul class="no-list-style">
                                                <li><a href="{{route('client.dashboard')}}"><i class="fal fa-chart-line"></i>{{__('cms.dashboard')}}</a></li>
                                                <li><a href="{{route('client.edit')}}"><i class="fal fa-user-edit"></i> {{__('cms.edit-profile')}} </a></li>
                                                <li><a href="{{route('client.show.change.password')}}" class="user-profile-act"><i class="fal fa-key"></i> {{__('cms.change-password')}} </a></li>
                                            </ul>
                                        </div>
                                        <!-- user-profile-menu end-->
                                        <!-- user-profile-menu-->
                                        <div class="user-profile-menu">
                                            <h3>{{__('cms.advertisings')}}</h3>
                                            <ul  class="no-list-style">
                                                <li><a href="#"><i class="fal fa-th-list"></i> {{__('cms.all')}}  </a></li>
                                            </ul>
                                        </div>
                                        <!-- user-profile-menu end-->                                        
                                        <button class="logout_btn color2-bg">{{__('cms.logout')}} <i class="fas fa-sign-out"></i></button>
                                    </div>
                                </div>
                                <a class="back-tofilters color2-bg custom-scroll-link fl-wrap" href="#dash_menu">{{__('cms.return')}}<i class="fas fa-caret-up"></i></a>
                            </div>