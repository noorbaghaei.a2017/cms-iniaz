@extends('template.app')


@section('content')



 <!-- content-->
 <div class="content">
                 <!--  section  -->
                 <section class="parallax-section dashboard-header-sec gradient-bg" data-scrollax-parent="true">
                        <div class="container">
                            <!--Tariff Plan menu-->
                            <div   class="tfp-btn"><span>طرح تعرفه : </span> <strong>تمدید شده</strong></div>
                            <div class="tfp-det">
                                <p>شما در حال <a href="#">تمدید هستید </a> . برای مشاهده جزئیات یا به روزرسانی از لینک زیر استفاده کنید. </p>
                                <a href="#" class="tfp-det-btn color2-bg">جزئیات</a>
                            </div>
                            <!--Tariff Plan menu end-->
                            <div class="dashboard-header_conatiner fl-wrap dashboard-header_title">
                                <h1> {{__('cms.your-welcome')}}  : <span>{{$client->full_name}}</span></h1>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="dashboard-header fl-wrap">
                            <div class="container">
                                <div class="dashboard-header_conatiner fl-wrap">
                                    <div class="dashboard-header-avatar">
                                    @if(!$client->Hasmedia('images'))
                        <img src="{{asset('template/images/user-icon-2.jpg')}}" alt="">
                        @else

                        <img src="{{$client->getFirstMediaUrl('images')}}" alt="">
                       
                        @endif
                                        
                                        <a href="dashboard-myprofile.html" class="color-bg edit-prof_btn"><i class="fal fa-edit"></i></a>
                                    </div>
                                    <div class="dashboard-header-stats-wrap">
                                        <div class="dashboard-header-stats">
                                            <div class="swiper-container">
                                                <div class="swiper-wrapper">
                                                 
                                                  
                                                  
                                                  
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                    <!--  dashboard-header-stats-wrap end -->
                                    <a class="add_new-dashboard">رزو ها  <i class="fal fa-layer-plus"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="gradient-bg-figure" style="right:-30px;top:10px;"></div>
                        <div class="gradient-bg-figure" style="left:-20px;bottom:30px;"></div>
                        <div class="circle-wrap" style="left:120px;bottom:120px;" data-scrollax="properties: { translateY: '-200px' }">
                            <div class="circle_bg-bal circle_bg-bal_small"></div>
                        </div>
                        <div class="circle-wrap" style="right:420px;bottom:-70px;" data-scrollax="properties: { translateY: '150px' }">
                            <div class="circle_bg-bal circle_bg-bal_big"></div>
                        </div>
                        <div class="circle-wrap" style="left:420px;top:-70px;" data-scrollax="properties: { translateY: '100px' }">
                            <div class="circle_bg-bal circle_bg-bal_big"></div>
                        </div>
                        <div class="circle-wrap" style="left:40%;bottom:-70px;"  >
                            <div class="circle_bg-bal circle_bg-bal_middle"></div>
                        </div>
                        <div class="circle-wrap" style="right:40%;top:-10px;"  >
                            <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
                        </div>
                        <div class="circle-wrap" style="right:55%;top:90px;"  >
                            <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
                        </div>
                    </section>
                    <!--  section  end-->
                    <!--  section  -->
                    <section class="gray-bg main-dashboard-sec" id="sec1">
                  
                        <div class="container">
                          
                          @include('template.auth.menu')
                            <!-- dashboard-menu  end-->
                            <!-- dashboard content-->
                            <div class="col-md-9">
                            
        
                                <form action="{{route('client.change.password')}}" method="POST">
                               
                                    @csrf
                                    @method('PATCH')
                                    <!-- profile-edit-container--> 
                                <div class="profile-edit-container fl-wrap block_box">
                                @include('core::layout.alert-danger')
                                    <div class="custom-form">
                                        <div class="pass-input-wrap fl-wrap">
                                            <label>{{__('cms.current-password')}}  </label>
                                            <input type="password" class="pass-input" name="current_password" placeholder="" value=""/>
                                            <span class="eye"><i class="far fa-eye" aria-hidden="true"></i> </span>
                                        </div>
                                        <div class="pass-input-wrap fl-wrap">
                                            <label>{{__('cms.new-password')}}  </label>
                                            <input type="password" class="pass-input" name="new_password" placeholder="" value=""/>
                                            <span class="eye"><i class="far fa-eye" aria-hidden="true"></i> </span>
                                        </div>
                                       
                                        <button class="btn    color2-bg  float-btn"> {{__('cms.update')}}<i class="fal fa-save"></i></button>
                                    </div>
                                </div>
                                <!-- profile-edit-container end--> 
                                </form>                                   
                            </div>
                            <!-- dashboard content end-->
                        </div>
                    </section>
                    <!--  section  end-->
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->

@endsection  


@section('heads')

@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<link type="text/css" rel="stylesheet" href="{{asset('template/css/dashboard-style.css')}}">
@else

<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-dashboard-style.css')}}">
@endif

@endsection