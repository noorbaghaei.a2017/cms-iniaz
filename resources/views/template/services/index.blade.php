@extends('template.app')

@section('content')


    <!-- Page Breadcrumb -->
    <section class="page-breadcrumb">
        <div class="image-layer" style="background-image:url({{asset('template/images/background/1.png')}})"></div>
        <div class="container">
            <div class="clearfix">
                <div class="pull-right">
                </div>
                <div class="pull-left">

                </div>
            </div>
        </div>
    </section>
    <!-- End Page Breadcrumb -->

    <!-- Services Page Section -->
    <section class="services-page-section">
        <div class="container">
            <div class="row box-services">
@foreach($items as $item)
                <!-- Services Block Three -->
                <div class="services-block-three col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="image">
                            <a href="{{route('services.single',['service'=>convert_lang($item,LaravelLocalization::getCurrentLocale(),'slug')])}}">
                                @if(!$item->Hasmedia('images'))
                                    <img class="img-fluid" src="{{asset('img/no-img.gif')}}" alt="{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}" title="{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}" >
                                @else
                                    <img class="img-fluid" src="{{$item->getFirstMediaUrl('images')}}" alt="{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}" title="{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}" >
                                @endif

                            </a>
                        </div>
                        <div class="lower-content">
                            <div class="icon-box">
                                <span class="icon icon-heart1" style="color: #dbb16b"></span>
                            </div>
                            <h3><a href="{{route('services.single',['service'=>convert_lang($item,LaravelLocalization::getCurrentLocale(),'slug')])}}">{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}</a></h3>
                            <p class="text">
                                {!! convert_lang($item,LaravelLocalization::getCurrentLocale(),'text') !!}
                            </p>
                        </div>
                    </div>
                </div>

    @endforeach



            </div>
        </div>
    </section>
    <!-- End Services Page Section -->


    <!-- Services Section Two -->
    <section class="services-section-two style-two">
        <div class="container">

            <div class="row">

                <!-- Services Block -->
                <div class="services-block col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-box">
                        <div class="content">
                            <div class="icon-box">
                                <span class="icon"><img src="{{asset('template/images/icons/icon-1.png')}}" alt="" /></span>
                            </div>
                            <h3><a href="#">بهداشت دندان</a></h3>
                            <p>پرسنل استثنایی و با تجربه ما به بهبود سلامت دندان و زیباتر کردن لبخند شما کمک خواهند کرد.</p>
                        </div>
                    </div>
                </div>

                <!-- Services Block -->
                <div class="services-block col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-box">
                        <div class="content">
                            <div class="icon-box">
                                <span class="icon"><img src="{{asset('template/images/icons/icon-2.png')}}" alt="" /></span>
                            </div>
                            <h3><a href="#">پر کردن دندان</a></h3>
                            <p>پرسنل استثنایی و با تجربه ما به بهبود سلامت دندان و زیباتر کردن لبخند شما کمک خواهند کرد.</p>
                        </div>
                    </div>
                </div>

                <!-- Services Block -->
                <div class="services-block col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-box">
                        <div class="content">
                            <div class="icon-box">
                                <span class="icon"><img src="{{asset('template/images/icons/icon-3.png')}}" alt="" /></span>
                            </div>
                            <h3><a href="#">درمان ریشه دندان</a></h3>
                            <p>پرسنل استثنایی و با تجربه ما به بهبود سلامت دندان و زیباتر کردن لبخند شما کمک خواهند کرد.</p>
                        </div>
                    </div>
                </div>

                <!-- Services Block -->
                <div class="services-block col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-box">
                        <div class="content">
                            <div class="icon-box">
                                <span class="icon"><img src="{{asset('template/images/icons/icon-4.png')}}" alt="" /></span>
                            </div>
                            <h3><a href="#">درمان ریشه دندان</a></h3>
                            <p>پرسنل استثنایی و با تجربه ما به بهبود سلامت دندان و زیباتر کردن لبخند شما کمک خواهند کرد.</p>
                        </div>
                    </div>
                </div>

                <!-- Services Block -->
                <div class="services-block col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-box">
                        <div class="content">
                            <div class="icon-box">
                                <span class="icon"><img src="{{asset('template/images/icons/icon-5.png')}}" alt="" /></span>
                            </div>
                            <h3><a href="#">دندانپزشکی کودکان</a></h3>
                            <p>پرسنل استثنایی و با تجربه ما به بهبود سلامت دندان و زیباتر کردن لبخند شما کمک خواهند کرد.</p>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </section>
    <!-- End Services Section Two -->



@endsection
