<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Modules\Ad\Entities\Ad;
use Modules\Chat\Entities\Chat;
use Modules\Client\Entities\Client;
use Modules\Client\Entities\ClientRole;
use Modules\Core\Entities\Area;
use Modules\Core\Entities\City;
use Modules\Core\Entities\Country;
use Modules\Core\Entities\State;
use Modules\Core\Entities\User;
use Modules\Product\Entities\CartList;
use Modules\Product\Entities\Product;
use Illuminate\Support\Facades\Auth;

class AjaxController extends Controller
{
    public function verifyMobileSms($user,$code){
        $user=Client::whereToken($user)->first();
        if($user->code==$code){
            $user->update([
                'is_active'=>2,
                'expire'=>now()
            ]);

            auth('client')->loginUsingId($user->id);

            $msg="success";
            $data=[
                'result'=>1
            ];
            $status=200;
        }
        else{
            $msg="کد نامعتبر است";
            $data=[
                'result'=>null
            ];
            $status=400;
        }
        return response()->json(array('msg'=>$msg,'data'=>$data),$status);
    }

    public function verifyEmailCode($email,$code){
        $client=Client::whereEmail($email)->first();
        if($client->code==$code){
            $client->update([
                'is_active'=>2,
                'expire'=>now()
            ]);

            auth('client')->loginUsingId($client->id);

            $msg="success";
            $data=[
                'result'=>1
            ];
            $status=200;
        }
        else{
            $msg="کد نامعتبر است";
            $data=[
                'result'=>null
            ];
            $status=400;
        }
        return response()->json(array('msg'=>$msg,'data'=>$data),$status);
    }


    public function Login($user,$pass){

       
        if(Client::whereEmail($user)->first()){
            $client=Client::whereEmail($user)->first();
            if(Hash::check($pass,$client->password)){

                auth('client')->loginUsingId($client->id);
                $data=[
                    'result'=>true,
                    'msg'=>''
                ];
            }
           
        }
        elseif(Client::whereUsername($user)->first()){
            $client=Client::whereUsername($user)->first();
            if(Hash::check($pass,$client->password)){

                auth('client')->loginUsingId($client->id);
                $data=[
                    'result'=>true,
                ];
            }
           
        }

        else{

            $data=[
                'result'=>false
            ];
        }
        return response()->json(array('msg'=>'ok','data'=>$data),200);

    }

    public function Register($email,$password,$first,$last){

        $item=[
            'email'=>$email,
            'password'=>$password,
            'first'=>$first,
            'last'=>$last,

        ];

        $validator = Validator::make($item, [
            'email'=>'unique:clients|required|email',
            'password'=>'required|min:8',
            'first'=>'required',
            'last'=>'required'
        ]);

        if($validator->fails()){

            $errors=$validator->errors();

            $data=[
                'result'=>false,
                'errors'=>$errors
            ];
        }
        else{
            $code=5044;
        $client=Client::create([
            'email'=>$email,
            'first_name'=>$first,
            'last_name'=>$last,
            'password'=>Hash::make($password),
            'code'=>$code,
            'code_expire'=>now()->addMinutes(10),
            'role'=>ClientRole::whereTitle('user')->firstOrFail()->id,
            'token'=>tokenGenerate(),
        ]);

        $client->myCode()->create([
            'code'=>generateCode()
        ]);

        $client->update([
            'is_active'=>2,
            'expire'=>now()

        ]);

        $client->info()->create();

        $client->analyzer()->create();

        $client->company()->create();

        $client->seo()->create();


        $client->wallet()->create([
            'score'=>500
        ]);

        $client->cart()->create([
            'client'=>$client->id,
            'mobile'=>""
        ]);

       
        if($client){
            auth('client')->loginUsingId($client->id);
            $data=[
                'result'=>true,
            ];
        }

        else{

            $data=[
                'result'=>false,
            ];
        }
        }


    

        
        return response()->json(array('msg'=>'ok','data'=>$data),200);

    }

    public function verifyToken($token){

        if(Client::whereToken($token)->first()){

            $client=Client::whereToken($token)->first();
            auth('client')->loginUsingId($client->id);
            $data=[
                'result'=>true,
            ];
        }

        else{

            $data=[
                'result'=>false,
            ];
        }
        return response()->json(array('msg'=>'ok','data'=>$data),200);
    }
    public function verifyEmail($email){
        $code=numberCode();
        if((Client::whereEmail($email)->first())){
            $client=Client::whereEmail($email)->update([
                'code'=>$code,
                'code_expire'=>now()->addMinutes(10),
            ]);
            $items=[
                'result'=>true,
                'email'=>$email,
                'code'=>$code
            ];
            $status=200;
            $data=[
                'code'=>$code,
                'text'=>__('cms.verify-code'),
               
             ];
           sendCustomEmail($data,'emails.front.otp-email');
        }
        else{
            $items=[
                'result'=>false,
                'email'=>$email,
                'code'=>$code
            ];
            $status=200;
        }

    
        return response()->json(array('msg'=>'ok','data'=>$items),200);
    }


    public function loadCountries(){

        $item=Country::latest()->get()->toArray();

            $msg="success";
            $status=200;

        return response()->json(array('msg'=>$msg,'data'=>$item),$status);
    }
    public function loadStates($country){

        $item=State::latest()->where('country',$country)->get()->toArray();
        $msg="success";
        $status=200;

        return response()->json(array('msg'=>$msg,'data'=>$item),$status);
    }
    public function loadCities($state){

        $item=City::latest()->where('state',$state)->get()->toArray();

        $msg="success";
        $status=200;

        return response()->json(array('msg'=>$msg,'data'=>$item),$status);
    }
    public function loadAreas($city)
    {

        $item = Area::latest()->where('city', $city)->get()->toArray();

        $msg = "success";
        $status = 200;

        return response()->json(array('msg'=>$msg,'data'=>$item),$status);

    }
        public function countAd($ad){

        $item=Ad::where('token',$ad)->first();
        $item->analyzer->increment('view');

        $item=[
            'item'=>$item,
            'token'=>$ad
        ];

        $msg="success";
        $status=200;

        return response()->json(array('msg'=>$msg,'data'=>$item),$status);
    }
    public function incProduct($token){

       $client=Client::with('cart')->find(auth('client')->user()->id);
        $product=Product::with('price')->whereToken($token)->first();
       $data=CartList::whereCart($client->cart->id)->whereProduct($product->id)->first();

        $update=$data->update([
           'count'=>$data->count + 1,
            'price'=>$data->price + intval($product->price->amount)
       ]);
        $totalCart=CartList::whereCart($client->cart->id)->get();

        $sendPrice=$totalCart->sum('price') > 300000 ? 0 : 25000;
        $totalPrice=$totalCart->sum('price') + $sendPrice;
        if($update){
            $item=[
                'method'=>'increment',
                'current'=>$data,
                'item'=>$totalCart,
                'totalPrice'=>$totalPrice,
                'productPrice'=>$totalCart->sum('price'),
                'sendPrice'=>$sendPrice
            ];

            $msg="success";
            $status=200;
        }
        else{
            $item=[
                'method'=>'increment',
                'count'=>null,
                'item'=>null,
                'totalPrice'=>0,
                'productPrice'=>0,
                'sendPrice'=>0
            ];

            $msg="error";
            $status=500;
        }


        return response()->json(array('msg'=>$msg,'data'=>$item),$status);
    }

    public function decProduct($token){

        $client=Client::with('cart')->find(auth('client')->user()->id);
        $product=Product::with('price')->whereToken($token)->first();
        $data=CartList::whereCart($client->cart->id)->whereProduct($product->id)->first();

        $update=$data->update([
            'count'=>$data->count - 1,
            'price'=>$data->price - intval($product->price->amount)
        ]);
        $totalCart=CartList::whereCart($client->cart->id)->get();
        $sendPrice=$totalCart->sum('price') > 300000 ? 0 : 25000;
        $totalPrice=$totalCart->sum('price') + $sendPrice;
        if($update){
            $item=[
                'method'=>'decrement',
                'current'=>$data,
                'item'=>$totalCart,
                'totalPrice'=>$totalPrice,
                'productPrice'=>$totalCart->sum('price'),
                'sendPrice'=>$sendPrice
            ];

            $msg="success";
            $status=200;
        }
        else{
            $item=[
                'method'=>'decrement',
                'current'=>null,
                'item'=>null,
                'totalPrice'=>0,
                'productPrice'=>0,
                'sendPrice'=>0
            ];

            $msg="error";
            $status=500;
        }


        return response()->json(array('msg'=>$msg,'data'=>$item),$status);
    }
    public function removeProduct($token){

        $client=Client::with('cart')->find(auth('client')->user()->id);
        $product=Product::with('price')->whereToken($token)->first();
        $deleted=CartList::whereCart($client->cart->id)->whereProduct($product->id)->delete();
        $totalCart=CartList::whereCart($client->cart->id)->get();
        $sendPrice=$totalCart->sum('price') > 300000 ? 0 : 25000;
        $totalPrice=$totalCart->sum('price') + $sendPrice;
        if($deleted){
            $item=[
                'method'=>'remove',
                'result'=>$totalCart->count(),
                'item'=>$totalCart,
                'totalPrice'=>$totalPrice,
                'productPrice'=>$totalCart->sum('price'),
                'sendPrice'=>$sendPrice
            ];

            $msg="success";
            $status=200;
        }
        else{
            $item=[
                'method'=>'remove',
                'result'=>$totalCart->count(),
                'item'=>null,
                'totalPrice'=>0,
                'productPrice'=>0,
                'sendPrice'=>0
            ];

            $msg="error";
            $status=500;
        }


        return response()->json(array('msg'=>$msg,'data'=>$item),$status);
    }

    public function showMessage(Request $request){

        $items=Chat::where('client',auth('client')->user()->id)->get();


        foreach ($items as $item) {

            if($item->user!=null){
                $item->update([
                    'status'=>2
                ]);
            }


        }

        return response()->json(['result'=>$items],200);

    }
}
